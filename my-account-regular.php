<?php 
    global $current_user;
     get_currentuserinfo();
     
?>
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#my-reservations" data-toggle="tab">My Reservations</a></li>
  <li><a href="#my-reviews" data-toggle="tab">My Reviews</a></li>
</ul>

<div class="tab-content">
    <div class="tab-pane fade in active" id="my-reservations">
  
  <?php
/* ----------------------------------------------------------------------------*/

$reservations = getTableReservations( $current_user->ID );

// The Loop
if ( count( $reservations ) > 0 ) :

?>

 <div class="table-responsive" style="padding:10px;">
<table class="table table-hover">
        <thead>
          <tr>
            <th>Venue</th>
            <th>Table Number</th>
            <th>Date</th>
            <th width="90px">Actions</th>
          </tr>
        </thead>
        <tbody>
          
          
<?php

foreach($reservations as $reservation) :

?>


    <tr>
            <td><a href="<?php echo get_permalink( $reservation->post_id ); ?>"><?php echo get_the_title( $reservation->post_id ); ?></a></td>
            <td><?php echo $reservation->table_number; ?></td>
            <td><?php echo $reservation->sched_date; ?></td>
            <td><a href="#cancel">Cancel</a></td>
          </tr>
    
    
 <?php

endforeach;
?>
</tbody>
      </table>
</div>
<?php
else:
    echo "<center>You have not posted any review!</center>";
endif;

/* ----------------------------------------------------------------------------*/
?>
  
  
    </div>
    
     <div class="tab-pane fade" id="my-reviews">

<?php
/* ----------------------------------------------------------------------------*/

$comments = get_comments('user_id=' . $current_user->ID );

// The Loop
if ( count( $comments ) > 0 ) :

?>

 <div class="table-responsive" style="padding:10px;">
<table class="table table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Post</th>
            <th>Title</th>
            <th>Review</th>
            <th width="90px">Actions</th>
          </tr>
        </thead>
        <tbody>
          
          
<?php

foreach($comments as $comment) :

?>


    <tr>
            <td><?php echo $comment->comment_ID; ?></td>
            <td><?php echo get_post_type( $comment->comment_post_ID ) ?></td>
            <td><?php echo get_the_title( $comment->comment_post_ID ) ?></td>
            <td><?php echo $comment->comment_content; ?></td>
            <td><a href="">Edit</a> &middot; <a href="#delete">Delete</a></td>
          </tr>
    
    
 <?php

endforeach;
?>
</tbody>
      </table>
</div>
<?php
else:
    echo "<center>You have not posted any review!</center>";
endif;

/* ----------------------------------------------------------------------------*/
?>

  </div>
</div>
