<?php
/* Template Name: Venues */
?>
<?php get_header(); ?>


<?php
		if( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();
?>

           
  <div id="page-container" class="single">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-title">
                    <h3>
                        <?php the_title(); ?>
                    </h3>
                </div>


            </div>
        </div>
      
         <div class="row">
            <div class="col-md-8 main-content">
            
            <?php the_content(); ?>
                              
            </div>
            <div class="col-md-4">
                <?php get_sidebar(); ?>
            </div>
        </div>
     
      </div>
      

      
      
      
   </div>
   
   <?php
				endwhile;
		endif;
			?> 

<?php get_footer(); ?>

