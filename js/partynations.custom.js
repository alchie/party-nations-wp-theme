(function($){
    $(document).ready(function($){
        $('#search-place-input').focus(function() {
            if( $('#search-place-input').css('width') == '120px') {
                //$('#search-place-input').css({'width': "120px", 'display' : 'block' });
                //$('#search-place-input').fadeIn().focus();
                $('#search-place-input').animate({'width': "+=90px" }, 300);
            } else {
                $('#search-place-input').trigger('blur');
            }
        });
         $('#search-place-input').blur(function() {
            $(this).animate({'width': "-=90px" });
         });
         $('#search-place-button').click(function() {
            if( $('#search-place-input').val() != '') {
                $('#form-search-form').submit();
            }
         });
         $( "#datepickerTable" ).datepicker({
            minDate: new Date(),
         });
        
        var hide_panel_active = function() {
            $('.panel-active').each(function() {
                var id = $(this).attr('data-id');
                $('#venue_update_'+id).slideUp(function() {
                    $('#venue_details_'+id).slideDown();
                }).removeClass('panel-active');
            });
        };
        $('.btn-manage-venue').click(function() {
            var id = $(this).attr('data-id');
            hide_panel_active();
            $('#venue_details_'+id).slideUp('slow', function(){
                $('#venue_update_'+id).slideDown().addClass('panel-active');
            }).removeClass('panel-active');
            
            $('.input-'+id).change(function() {
                $('.submit-'+id).prop('disabled', false);
            });
        });
        
        $('.btn-venue-cancel').click(function() {
            var id = $(this).attr('data-id');
            hide_panel_active();
            $('#venue_update_'+id).slideUp('slow', function(){
                $('#venue_details_'+id).slideDown();
            }).removeClass('panel-active');
        });
        
        $('.btn-venue-update').click(function() {
            var id = $(this).attr('data-id'), 
            nonce = $(this).attr('data-nonce'), 
            venue_title = $('#venue_title_'+id).val(),
            venue_description = $('#venue_description_'+id).val(),
            venue_location = $('#venue_location_'+id).val(),
            venue_opening = $('#venue_opening_'+id).val(),
            venue_happy = $('#venue_happy_'+id).val(),
            venue_website = $('#venue_website_'+id).val(),
            venue_phone = $('#venue_phone_'+id).val(),                        
            venue_tables = $('#venue_tables_'+id).val()                                    
            ;
            $('.input-'+id).prop('disabled', true);

            $.ajax({
                type : 'POST',
                url: ajax_url,
                data : { 
                    action      :   'update_venue',
                    id          :   id,
                    nonce       :   nonce,
                    title       :   venue_title,
                    description :   venue_description,
                    location    :   venue_location,
                    opening     :   venue_opening,
                    happy       :   venue_happy,
                    website     :   venue_website,
                    phone       :   venue_phone,
                    tables      :   venue_tables,
                }                
            }).done(function(msg) {
                if( msg == 1 ) { 
                    $('.venue-title-'+id).text( venue_title );
                    $('.venue-description-'+id).text( venue_description );
                    alert('Successfully Updated!');
                } else {
                    alert('Error Occured!');                
                }               
               $('.input-'+id).prop('disabled', false);
            });
            
        });
    });
    $(window).scroll( function() {
        event.preventDefault();
        if( ( $("body").scrollTop() > 500 ) || ( $("html").scrollTop() > 500 ) ) {
            if( $('#backToTop').length == 0 ) {
                $('body').append('<a id="backToTop" href="javascript:void(0);"><span class="glyphicon glyphicon-chevron-up"></span></a>');
                $('#backToTop').fadeIn('slow');
                $('#backToTop').click(function(event) {
                    event.preventDefault();
                    $('html, body').animate({scrollTop: 0}, 500);
                    return false;
                 });
            }
        } else {
            $('#backToTop').remove();
        }
       
    });
})(jQuery);

