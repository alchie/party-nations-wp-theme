<?php
/* Template Name: Events Archive */
?>
<?php get_header(); ?>

  <div id="events-container" class="template">

   <?php
	global $post;
	 $envira = new Envira_Gallery_Lite; 
    $gallery = $envira->get_gallery($post->ID);
    
    if( isset( $gallery['gallery'] ) && ( count( $gallery['gallery'] ) > 0) ) { 
    
  ?>
  
 <div id="bg-slider-container" class="carousel slide bg-header" data-ride="carousel">
  <div class="container">
        <ol class="carousel-indicators">
        <?php $n=0; foreach( $gallery['gallery'] as $id => $item ) { ?>
          <li data-target="#bg-slider-container" data-slide-to="<?php echo $n; ?>" <?php echo ($n==0) ? 'class="active"' : ''; ?>></li>
         <?php $n++; } ?>
        </ol>
        <div class="carousel-inner">
        <?php $n=0; foreach( $gallery['gallery'] as $id => $item ) { ?>
          <div class="item <?php echo ($n==0) ? 'active' : ''; ?>">
            <img src="<?php echo $item['src']; ?>" id="<?php echo $id; ?>" title="<?php echo $item['title']; ?>" alt="<?php echo $item['alt']; ?>">
          </div>
        <?php $n++; } ?>
        </div>
        <a class="left carousel-control" href="#bg-slider-container" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#bg-slider-container" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
</div>
      </div>
   <?php } else { ?>
     <div class="bg"></div>
   <?php } ?>

     <div class="container">
       <div class="row">
        <div class="col-md-12 box-header">
 <?php
		if( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();
?>

 <h3 class="box-title"><?php the_title(); ?></h3>
            <div class="text-center"><?php the_content(); ?></div>
            
<?php
				endwhile;
		endif;
			?>        </div>
       </div> 
     </div>
     
     
<div class="container">
    <div class="row">
        <div class="col-md-12">
        
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">Events</li>
</ol>

           
        </div>
    </div>
</div>


    <div class="events-list container">
   <div class="row">
        <div class="col-md-9">
<?php

// The Query
query_posts( array(
    'post_type' => 'event',
    'posts_per_page' => 5 ,
    'paged' => max( 1, get_query_var('paged') ),
    'orderby' => 'meta_value',
    'meta_key' => 'event_datetime',
    'order' => ASC,
    'meta_query' => array(
		array(
			'key' => 'event_datetime',
			'value' => date('m/d/Y H:i:s'),
			'compare' => '<='
		)
	)
) );



// The Loop
if ( have_posts() ) :

while ( have_posts() ) : the_post();


?>
   
 
            <div class="item">
                <div class="row">
                    <div class="col-md-3 event-thumbnail">
   <?php
                       
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('medium', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:100%">
                        <?php
                        }
     ?>
                      
                    </div>
                    <div class="col-md-4 details">
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h3 class="panel-title"><?php the_title(); ?></h3>
                          </div>
                          <div class="panel-body">
                            
                            <p><span class="label">Venue:</span> <?php if( get_post_meta(get_the_ID(), 'event_venue', true) ) { ?><a href="<?php echo get_permalink( get_post_meta(get_the_ID(), 'event_venue', true) ); ?>"><?php echo get_the_title( get_post_meta(get_the_ID(), 'event_venue', true) ); ?></a><?php } ?></p>
                            <p><span class="label">City:</span> <?php echo get_post_meta(get_the_ID(), 'event_city', true); ?></p>
                            <p><span class="label">Age Limit:</span> <?php echo get_post_meta(get_the_ID(), 'event_age_limit', true); ?></p>   
                            <p><span class="label">Services:</span> <?php echo get_post_meta(get_the_ID(), 'event_services', true); ?></p>     
                            <p><span class="label">Date &amp; Time:</span> <?php echo get_post_meta(get_the_ID(), 'event_datetime', true); ?></p> 
                             
                              
                            
                          </div>
                          <div class="panel-footer">
                            <a href="<?php the_permalink(); ?>" class="btn btn-danger btn-sm">Show Description <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
                          </div>
                        </div>                    
                    </div>
                    <div class="col-md-5 countdown">
                    
                    <div class="panel panel-default">
                          <div class="panel-heading">
                          <?php $countdown = eventDateCountdown(get_post_meta(get_the_ID(), 'event_datetime', true)); ?>
                            <div class="box"><?php echo  $countdown['days']; ?> <span class="label">Days</span></div>
                            <div class="box"><?php echo  $countdown['hours']; ?> <span class="label">Hours</span></div>
                            <div class="box"><?php echo  $countdown['mins']; ?> <span class="label">Min</span></div>
                          </div>
                          <div class="panel-body">
                            <i class="fa fa-calendar calendar-icon"></i> <?php echo date('l jS \of F Y h:i:s A', strtotime( get_post_meta(get_the_ID(), 'event_datetime', true) )) . "\n"; ?>
                          </div>
                          <div class="panel-footer">
                          
                          <?php get_template_part('social', 'media'); ?>

                          </div>
                        </div>                    
                    
                    </div>
                </div>
           
             </div>
       
<?php
endwhile;

?>

    
    
    <!-- pagination -->
    <div class="row">
        <div class="col-md-12">
        
            <ul class="pagination">
             
              <?php
 
 global $wp_query;

$big = 999999999; // need an unlikely integer

$pagination = paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $wp_query->max_num_pages,
	'type' => 'array'
) );
if( $pagination ) {
foreach($pagination as $page) {
    echo "<li>" . $page ."</li>";
}
}
?>
             
              

            </ul>
        </div>
    </div>
       <!-- end pagination -->

 <?php

endif;
// Reset Query
wp_reset_query();



?> 


 </div>
 <div class="col-md-3">
                <?php get_sidebar('event'); ?>
            </div>
      </div>

    </div>

    
    
   
 </div>
       
       
 <?php get_template_part('footer', 'partners'); ?>

<?php get_footer(); ?>

