<?php
/* Template Name: Venues */
?>
<?php get_header(); ?>


<?php
		if( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();
?>

           
  <div id="news-single-container" class="single">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-title">
                    <h3>
                        News
                    </h3>
                </div>
                
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="/news-archive/">News</a></li>
  <li class="active"><?php the_title(); ?></li>
</ol>


            </div>
        </div>
        <div class="post">
         <div class="row">
            <div class="col-md-12"> 
                <h3 class="post-title"><?php the_title(); ?></h3>
                <p class="meta"><strong>Posted By</strong> <a href="<?php echo get_the_author_meta('user_url', $post->post_author);?>"><?php echo get_the_author_meta('display_name', $post->post_author);?></a> - <?php the_date('M d, Y'); ?>
            </div>
        </div>
        
         <div class="row">
            <div class="col-md-9">
               
                        <div class="thumbnail">
                             <?php
                       
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('medium', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:100%">
                        <?php
                        }
     ?>
                       </div>
                
                 <?php get_template_part('social', 'media'); ?>
<hr >

<?php the_content(); ?>

<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="750" data-numposts="5" data-colorscheme="light"></div>
                
            </div>
            <div class="col-md-3">
                <?php get_sidebar('news'); ?>
            </div>
        </div>
        </div>
      </div>
      

      
      
      
   </div>
   
   <?php
				endwhile;
		endif;
			?> 

<?php get_footer(); ?>

