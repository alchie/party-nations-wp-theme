<?php
/* Template Name: Venues */
?>
<?php get_header(); ?>



  <div id="venues-container" class="template">
  
  
   <?php
	global $post;
	 $envira = new Envira_Gallery_Lite; 
    $gallery = $envira->get_gallery($post->ID);
    
    if( isset( $gallery['gallery'] ) && ( count( $gallery['gallery'] ) > 0) ) { 
    
  ?>
  
 <div id="bg-slider-container" class="carousel slide bg-header" data-ride="carousel">
  <div class="container">
        <ol class="carousel-indicators">
        <?php $n=0; foreach( $gallery['gallery'] as $id => $item ) { ?>
          <li data-target="#bg-slider-container" data-slide-to="<?php echo $n; ?>" <?php echo ($n==0) ? 'class="active"' : ''; ?>></li>
         <?php $n++; } ?>
        </ol>
        <div class="carousel-inner">
        <?php $n=0; foreach( $gallery['gallery'] as $id => $item ) { ?>
          <div class="item <?php echo ($n==0) ? 'active' : ''; ?>">
            <img src="<?php echo $item['src']; ?>" id="<?php echo $id; ?>" title="<?php echo $item['title']; ?>" alt="<?php echo $item['alt']; ?>">
          </div>
        <?php $n++; } ?>
        </div>
        <a class="left carousel-control" href="#bg-slider-container" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#bg-slider-container" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
</div>
      </div>
   <?php } else { ?>
     <div class="bg"></div>
   <?php } ?>
    
    
     <div class="container">
       <div class="row">
        <div class="col-md-12 box-header">

 <?php
		if( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();
?>

 <h3 class="box-title"><?php the_title(); ?></h3>
            <div class="text-center"><?php the_content(); ?></div>
            
<?php
				endwhile;
		endif;
			?>        </div>
       </div> 
     </div>
     
     
<div class="container">
    <div class="row">
        <div class="col-md-12">
        
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">Events</li>
</ol>


        </div>
    </div>
</div>


    <div class="venue-list container">
    
      <div class="row">
        <div class="col-md-9">
        
<div class="row">
<div class="col-md-12">
        
        <form method="get">
<div class="filter pull-right">
    <span>Filter by Rating</span> 
    <select name="rating" class="input-sm">
     <option value="">ALL</option>
    <?php $ratings = array(5,4,3,2,1);
    foreach($ratings as $rate) {
       echo '<option value="'.$rate.'" ';
          if( $rate == $_GET['rating']) echo ' SELECTED ';
       echo '>'. $rate .'</option>';
     } 
     ?>           
                    
    </select>
    <button type="submit" class="btn btn-danger btn-xs">Search</button>
</div>

<div class="filter pull-right">
    <span>Filter by Location</span> 
    <select name="location" class="input-sm">
    <option value="">ALL</option>
    <?php $locations = get_terms('location'); 
    foreach($locations as $loc) {
       echo '<option value="'.$loc->term_id.'" ';
       if( $loc->term_id == $_GET['location']) echo ' SELECTED ';
       echo '>'.$loc->name.'</option>';
     } ?>
    </select>
    <button type="submit" class="btn btn-danger btn-xs">Search</button>
</div>
</form>
<div class="clearfix"></div>
<br />
</div>
</div>

<?php

$args = array(
    'post_type' => 'venue',
    'posts_per_page' => 5,
    'paged' => max( 1, get_query_var('paged') ),
    
);

if( isset($_GET['location']) && $_GET['location'] != '' ) {
   
		$args['tax_query'][] = array(
			'taxonomy' => 'location',
			'field' => 'term_id',
			'terms' =>  $_GET['location']
		);
	
}
if( isset($_GET['rating']) && $_GET['rating'] != '' ) {

	$args['meta_query'][] = array(
			'key' => '_venue_rating',
			'value' => $_GET['rating'],
			'compare' => '=='
		);

}

// The Query
query_posts( $args );

// The Loop
if ( have_posts() ) :

while ( have_posts() ) : the_post();

?>


    
 
            <div class="item">
                <div class="row">
                    <div class="col-md-3">
                        <div class="thumbnail">
<a href="<?php the_permalink(); ?>">
   <?php
                       
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('medium', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:100%">
                        <?php
                        }
     ?>
     </a>
     </div>
                    </div>
                    <div class="col-md-5 details">
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h3 class="panel-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                          </div>
                          <div class="panel-body">
                                     <?php the_excerpt(); ?>            
                          </div>
                          <div class="panel-footer">
                            <a href="<?php the_permalink(); ?>" class="btn btn-danger btn-sm">Show Description <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
                          </div>
                        </div>                    
                    </div>
                    <div class="col-md-4 extra details">
                    
                    <div class="panel panel-default">
                          <div class="panel-heading">
                          <h3 class="panel-title">Venue Details</h3>
                          </div>
                          <div class="panel-body">
                            <p></p>
                            <?php $location = wp_get_post_terms( get_the_ID(), 'location', array("fields" => "names") ); ?>
                            <p><span class="label">Location:</span> <br><?php echo implode(', ', $location);  ?></p>
                            <p><span class="label">Opening Hours:</span> <br><?php echo get_post_meta(get_the_ID(), 'venue_opening', true); ?></p>
                            <p><span class="label">Happy Hours:</span> <br><?php echo get_post_meta(get_the_ID(), 'venue_happy', true); ?></p>   
                               <p><span class="label">Rating:</span> </p> <?php the_rating(); ?>
                          </div>

                        </div>                    
                    
                    </div>
                </div>
            </div>
            
    
            
        
        
               
      

<?php
endwhile;

else:
    echo "<center>No Match Found!</center>";
endif;
?>
    </div>
         <div class="col-md-3">
                <?php get_sidebar('venue'); ?>
            </div>
        
        
      </div>

    </div>

    
    <!-- pagination -->
    <div class="container">
    <div class="row">
        <div class="col-md-12">
        
            <ul class="pagination">
             
              <?php
 
 global $wp_query;

$big = 999999999; // need an unlikely integer

$pagination = paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $wp_query->max_num_pages,
	'type' => 'array'
) );
if( $pagination ) {
foreach($pagination as $page) {
    echo "<li>" . $page ."</li>";
}
}
?>
             
              

            </ul>
        </div>
    </div>
</div>
       <!-- end pagination -->

<?php


// Reset Query
wp_reset_query();

?>  
   
    
 </div>
       
       
       <?php get_template_part('footer', 'partners'); ?>

<?php get_footer(); ?>

