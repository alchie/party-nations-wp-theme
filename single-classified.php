<?php
/* Template Name: Classifieds Front */
?>
<?php get_header(); ?>

  <div id="classifieds-container" class="template">
  
  
  
   <?php
	global $post;
	 $envira = new Envira_Gallery_Lite; 
    $gallery = $envira->get_gallery($post->ID);
    
    if( isset( $gallery['gallery'] ) && ( count( $gallery['gallery'] ) > 1) ) { 
    
  ?>
  
 <div id="bg-slider-container" class="carousel slide bg-header" data-ride="carousel">
  <div class="container">
        <ol class="carousel-indicators">
        <?php $n=0; foreach( $gallery['gallery'] as $id => $item ) { ?>
          <li data-target="#bg-slider-container" data-slide-to="<?php echo $n; ?>" <?php echo ($n==0) ? 'class="active"' : ''; ?>></li>
         <?php $n++; } ?>
        </ol>
        <div class="carousel-inner">
        <?php $n=0; foreach( $gallery['gallery'] as $id => $item ) { ?>
          <div class="item <?php echo ($n==0) ? 'active' : ''; ?>">
            <img src="<?php echo $item['src']; ?>" id="<?php echo $id; ?>" title="<?php echo $item['title']; ?>" alt="<?php echo $item['alt']; ?>">
          </div>
        <?php $n++; } ?>
        </div>
        <a class="left carousel-control" href="#bg-slider-container" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#bg-slider-container" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
</div>
      </div>
   <?php } else { ?>
     <div class="bg"></div>
   <?php } ?>
   
   
     <div class="container">
       <div class="row">
        <div class="col-md-12 box-header">
 <?php $promos = get_page( of_get_option('partynations_classified_page') ); ?>

 <h3 class="box-title"><?php echo $promos->post_title; ?></h3>
            <div class="text-center"><?php echo $promos->post_content; ?></div> 
            
             </div>
       </div> 
     </div>
     
<div class=" container">
    <div class="row">
        <div class="col-md-12">
        
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>

    <li><a href="/classifieds/">Classifieds</a></li>
     <li class="active"><?php the_title(); ?></li>
 
  
</ol>
 


<?php if( of_get_option('partynations_classified_category') ) { ?>
 <ul class="nav nav-tabs">
 <?php $classcat = get_categories('hide_empty=0&child_of=' . of_get_option('partynations_classified_category') ); ?>
 <?php foreach($classcat as $cc) { ?>
    <li class="<?php echo ($_GET['cat'] ==  $cc->term_id ) ? 'active' : ''; ?>"><a href="/classifieds/?cat=<?php echo $cc->term_id; ?>"><?php echo $cc->cat_name; ?></a></li>
<?php } ?>
  <ul>
<?php } ?>


        </div>
    </div>
</div>

      
       <div class="classified-list container">
      <div class="row">
      
      
      <div class="col-md-8">
 
            <div class="panel panel-default panel-current" >
              <div class="panel-heading">
                <h3 class="panel-title"><?php the_title(); ?></h3>
                 <span class="arrow"></span>
              </div>
              <div class="panel-body">
                <?php echo substr(get_the_excerpt(), 0, 140); ?>
              </div>
              <div class="panel-footer">
                    
              </div>
            </div>
        </div>
        
      
<?php
$i=0;

query_posts( array(
    'post_type' => 'classified',
    'posts_per_page' => 8,
    'cat' =>    $_GET['cat'],
    'post__not_in' => array(get_the_ID()),
) );

		if( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();
?>
       <div class="col-md-4">
 
            <div class="panel panel-default panel-<?php echo $i; ?>">
              <div class="panel-heading">
                <h3 class="panel-title"><?php the_title(); ?></h3>
                 <span class="arrow"></span>
              </div>
              <div class="panel-body">
                <?php echo substr(get_the_excerpt(), 0, 140); ?>
              </div>
              <div class="panel-footer">
                    
                    <a href="<?php the_permalink(); ?>" class="btn btn-danger btn-sm btn-item pull-right">Read More <i class="glyphicon glyphicon-circle-arrow-right"></i></a>
                    <div class="clearfix"></div>
              </div>
            </div>
        </div>
     
    <?php
   $i++;
				endwhile;
		
		else:
		
		    echo '<center>No '.  $term->cat_name .' Classified Found!</center>';		
				
		endif;
		?>
      

      </div>
    </div>
   


   
    
 </div>
       
       
<?php get_template_part('footer', 'partners'); ?>

<?php get_footer(); ?>

