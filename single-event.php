<?php
/* Template Name: Venues */
?>
<?php get_header(); ?>



           
  <div id="venue-single-container" class="single">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-title">
                    <h3>
                        Event
                    </h3>
                </div>
                

           <!--
               <div class="prev-next-container">
                    <div class="prev col-md-6">
<?php
$p = eventPrevPost();
if(!empty($p)) { //echo '<a class="prevpost" href="'.$p->guid.'" title="'.$p->post_title.'">&nbsp</a>';
?>
                    <a href="<?php echo $p->guid; ?>">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <h4>PREV EVENT</h4>
                        <p><?php echo $p->post_title; ?></p>
                    </a>
<?php } ?>                    
                    </div>
                    <div class="next col-md-6 ">
<?php
$n = eventNextPost();
if(!empty($n))  { //echo '<a class="nextpost" href="'.$n->guid.'" title="'.$n->post_title.'">&nbsp</a>';
?>
                    <a href="<?php echo $n->guid; ?>">
                        <span class="glyphicon glyphicon-chevron-right"></span>      
                        <h4>NEXT EVENT</h4>
                        <p><?php echo $n->post_title; ?></p>       
                    </a>
<?php } ?>
                    </div>

                    <div class="clearfix"></div>
                </div>                    
 
-->
 

      
 <?php
		if( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();
?>          
                
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="/events/">Events</a></li>
  <li class="active"><?php the_title(); ?></li>
</ol>


            </div>
        </div>
        <div class="post">

        
         <div class="row">
            <div class="col-md-9">
            
            <h3 class="post-title"><?php the_title(); ?></h3>
                <div class="venue-details">
                    
                        <div class="col-md-5 thumbnail">
                             <?php
                       
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('medium', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:100%">
                        <?php
                        }
                        
     ?>
                        </div>
                        
                     <div class="col-md-7">
                     <h3>Event Details</h3>
                     
                     <p><span class="label">Venue:</span> <?php if( get_post_meta(get_the_ID(), 'event_venue', true) ) { ?><a href="<?php echo get_permalink( get_post_meta(get_the_ID(), 'event_venue', true) ); ?>"><?php echo get_the_title( get_post_meta(get_the_ID(), 'event_venue', true) ); ?></a><?php } ?></p>
                            <p><span class="label">City:</span> <?php echo get_post_meta(get_the_ID(), 'event_city', true); ?></p>
                            <p><span class="label">Age Limit:</span> <?php echo get_post_meta(get_the_ID(), 'event_age_limit', true); ?></p>   
                            <p><span class="label">Services:</span> <?php echo get_post_meta(get_the_ID(), 'event_services', true); ?></p>     
                            <p><span class="label">Date &amp; Time:</span> <?php echo get_post_meta(get_the_ID(), 'event_datetime', true); ?></p> 
                             
                              
                     </div>
                     <div class="clearfix"></div>
                </div>
                
                

<?php the_content(); ?>

                
            </div>
            <div class="col-md-3">
                <?php get_sidebar('event'); ?>
            </div>
        </div>
        </div>
      </div>
      

      
      
      
   </div>
   
   <?php
				endwhile;
		endif;
			?> 

<?php get_footer(); ?>

