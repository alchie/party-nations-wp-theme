<?php
/* Template Name: Promotions */
?>
<?php get_header(); ?>

  <div id="promotions-container" class="template">
  
    <div class="bg"></div>
     <div class="container">
       <div class="row">
 <div class="col-md-12 box-header">
 
	<?php $promo_title =  get_post_meta(get_the_ID(), 'promo_header_title', true); ?>

 <h3 class="box-title"><?php echo ($promo_title) ? $promo_title : 'Promotion'; ?></h3>
            <div class="text-center"></div>
            

			</div>
       </div> 
     </div>
     
      <?php
		if( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();
?>    

<div class="container">
    <div class="row">
        <div class="col-md-12">
        
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="/promotions/">Promotions</a></li>
  <li class="active"><?php the_title(); ?></li>
</ol>


        </div>
    </div>
</div>




    <div id="promotions-container-single">
        <div class="container">
            <div class="row">
                <div class="col-md-12 promo-image">
                
                 <div class="thumbnail">
                             <?php
                       
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('full', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:100%">
                        <?php
                        }
                        
     ?>
                        </div>
                
                </div>
            </div>
        </div>        
         <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h3 class="promo-title"><?php the_title(); ?></h3>
                    <div class="promo-content"> <?php the_content(); ?></div>
                </div>
                <div class="col-md-5">
                    <div class="gray-box">
                        <p><span class="label">Venue:</span> <?php if( get_post_meta(get_the_ID(), 'promo_venue', true) ) { ?><a href="<?php echo get_permalink( get_post_meta(get_the_ID(), 'promo_venue', true) ); ?>"><?php echo get_the_title( get_post_meta(get_the_ID(), 'promo_venue', true) ); ?></a><?php } ?></p>
                        <p><span class="label">Date:</span> <?php echo get_post_meta(get_the_ID(), 'promo_datetime', true); ?></p>
                        <p><span class="label">Website:</span> <?php echo get_post_meta(get_the_ID(), 'promo_website', true); ?></p>
                    </div>
                </div>
            </div>
        </div>  
    </div>


    <div id="more-promotions-container">
        
         <div class="container">
            <div class="row">
                <div class="col-md-12">
                
                <h3>View Other Promotions</h3>
                            <div id="carousel-more-promo" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carousel-more-promo" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-more-promo" data-slide-to="1"></li>
          <li data-target="#carousel-more-promo" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="item active">

<?php
// The Query
global $post;
$main_id = $post->ID;
query_posts( array(
    'post_type' => 'promotion',
    'posts_per_page' => 13
) );

// The Loop
if ( have_posts() ) :

$n=4;
$c=0;
while ( have_posts() ) : the_post();


if( ( $c < 12 )  ) {
if ($main_id != get_the_ID()) {
?>

            <div class="item-box col-md-3">
            <div class="thumbnail">
            <a href="<?php the_permalink(); ?>">
                 <?php
                       
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('medium', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:100%">
                        <?php
                        }
     ?>
            </a>
            </div>
            </div>

<?php
}
$n--;
$c++;

}

if( ($n == 0) && ($c < 12)) {
    $n = 4;
    echo '</div><div class="item">';
}

endwhile;

endif;

// Reset Query
wp_reset_query();
?>

            
          </div>
          
        </div>
        <a class="left carousel-control" href="#carousel-more-promo" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-more-promo" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>
                
    <?php
				endwhile;
		endif;
			?>  
			            
                </div>
            </div>
        </div>  
        
    </div>
    
 </div>
       
       
<?php get_template_part('footer', 'partners'); ?>

<?php get_footer(); ?>

