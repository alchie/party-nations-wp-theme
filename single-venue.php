<?php
/* Template Name: Venues */
?>
<?php get_header(); ?>


<?php
		if( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();
?>

           
  <div id="venue-single-container" class="single">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-title">
                    <h3>
                        Event
                    </h3>
                </div>
                

                <div class="prev-next-container">
                    <div class="prev col-md-6">
<?php
$p = get_adjacent_post(false, '', 1);
if(!empty($p)) { //echo '<a class="prevpost" href="'.$p->guid.'" title="'.$p->post_title.'">&nbsp</a>';
?>
                    <a href="<?php echo $p->guid; ?>">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <h4>PREV VENUE</h4>
                        <p><?php echo $p->post_title; ?></p>
                    </a>
<?php } ?>                    
                    </div>
                    <div class="next col-md-6 ">
<?php
$n = get_adjacent_post(false, '', 0);
if(!empty($n))  { //echo '<a class="nextpost" href="'.$n->guid.'" title="'.$n->post_title.'">&nbsp</a>';
?>
                    <a href="<?php echo $n->guid; ?>">
                        <span class="glyphicon glyphicon-chevron-right"></span>      
                        <h4>NEXT VENUE</h4>
                        <p><?php echo $n->post_title; ?></p>       
                    </a>
<?php } ?>
                    </div>

                    <div class="clearfix"></div>
                </div>                
                
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="/venues/">Venue</a></li>
  <li class="active"><?php the_title(); ?></li>
</ol>


            </div>
        </div>
        <div class="post">
         
        
         <div class="row">
            <div class="col-md-9">
            <div class="pull-right">
                <iframe src="//www.facebook.com/plugins/like.php?href=<?php echo urlencode( get_permalink() ); ?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21&amp;appId=445371815498377" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px; float:left;width: 100px;" allowTransparency="true"></iframe>
            </div>
            <div class="pull-left">
            <h3 class="post-title"><?php the_title(); ?></h3>
            </div>
            
            <div class="clearfix"></div>
            
                <div class="venue-details">
                    
                        <div class="col-md-6 thumbnail">
                             <?php
                       
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('medium', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:100%">
                        <?php
                        }
                        
     ?>
                        </div>
                        
                     <div class="col-md-6">
                     <h3>Venue Details</h3>
                     
                     <?php $location = wp_get_post_terms( get_the_ID(), 'location', array("fields" => "names") ); ?>
                            <p><span class="label">Location:</span> <?php echo implode(', ', $location);  ?></p>
                            <p><span class="label">Opening Hours:</span> <?php echo get_post_meta(get_the_ID(), 'venue_opening', true); ?></p>
                            <p><span class="label">Happy Hours:</span> <?php echo get_post_meta(get_the_ID(), 'venue_happy', true); ?></p>   
                            <p><span class="label">Website:</span> <a href="<?php echo get_post_meta(get_the_ID(), 'venue_website', true); ?>"><?php echo get_post_meta(get_the_ID(), 'venue_website', true); ?></a></p>     
                            <p><span class="label">Phone Number:</span> <?php echo get_post_meta(get_the_ID(), 'venue_phone', true); ?></p> 
                             <p><span class="label">Rating:</span>  <?php the_rating(); ?></p> 
                             
                              
                     </div>
                     <div class="clearfix"></div>
                     
                    
                </div>
                
                 <div class="row" style="margin:10px 0;">
                    <div class="col-md-8">
                        <?php //get_template_part('social', 'media'); ?>
                    </div>
                    <div class="col-md-4">
                        <!--<a href="#venue-tables" data-toggle="tab" class="btn btn-danger btn-sm btn-block">Reserve a Table</a>-->
                    </div>
                 </div>
                
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="<?php echo (isset($_POST['action']) && $_POST['action'] == 'search_table') ? '' : 'active'; ?>"><a href="#venue-description" data-toggle="tab">Description</a></li>
  <li><a href="#venue-media" data-toggle="tab">Photos &amp; Videos</a></li>
    <li><a href="#venue-reviews" data-toggle="tab">Reviews</a></li>
      <li><a href="#venue-promotions" data-toggle="tab">Promotions</a></li>
        <li><a href="#venue-events" data-toggle="tab">Events</a></li>
                <li><a href="#venue-news" data-toggle="tab">News</a></li>
</ul>

<div class="tab-content">
  <div class="tab-pane fade <?php echo (isset($_POST['action']) && $_POST['action'] == 'search_table') ? '' : 'in active'; ?>" id="venue-description"><?php the_content(); ?>
  
  <?php 
  //if( class_exists() ) {
    echo do_shortcode('[codepeople-post-map]'); 
  //}
  ?>
  
  
  </div>
  <div class="tab-pane fade" id="venue-media">
  <?php
  if ( function_exists( 'envira_gallery' ) ) { 
  
 envira_gallery2( get_the_ID() );
  
  }
  ?>
  </div>
 
  <div class="tab-pane fade" id="venue-reviews">
<?php
if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
?>
  </div>
   <div class="tab-pane fade list" id="venue-promotions">


<?php

$n = 0;
// The Query
$query = new WP_Query( array(
    'post_type' => 'promotion',
    'posts_per_page' => 5,
    'meta_key' => 'promo_venue',
    'meta_query' => array(
       array(
           'key' => 'promo_venue',
           'value' => $post->ID,
           'compare' => 'IN',
       )
   )
) );

// The Loop
if ( $query->have_posts() ) :

while ( $query->have_posts() ) : $query->the_post();
?>
<div class="row <?php echo $even = (($n % 2) == 0) ? 'even' : 'odd'; ?>">
<div class="col-md-2">
<div class="thumbnail">
<a href="<?php the_permalink(); ?>">
<?php
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('thumbnail', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="img-responsive" style="width:100%">
                        <?php
                        }
?>
</a>
</div>
</div>
<div class="col-md-10">
    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
      <?php the_content(); ?>
</div>
</div>
<?php
$n++;
endwhile;
else:
    echo "<center>No Promotion Found!</center>";

endif;

// Reset Query
wp_reset_query();

?>


  </div>
     <div class="tab-pane fade list" id="venue-events">


<?php

$e = 0;
// The Query
$query = new WP_Query( array(
    'post_type' => 'event',
    'posts_per_page' => 5,
    'meta_key' => 'event_venue',
    'meta_query' => array(
       array(
           'key' => 'event_venue',
           'value' => $post->ID,
           'compare' => 'IN',
       )
   )
) );

// The Loop
if ( $query->have_posts() ) :

while ( $query->have_posts() ) : $query->the_post();
?>
<div class="row <?php echo $even = (($e % 2) == 0) ? 'even' : 'odd'; ?>">
<div class="col-md-2">
<div class="thumbnail">
<a href="<?php the_permalink(); ?>">
<?php
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('thumbnail', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="img-responsive" style="width:100%">
                        <?php
                        }
?>
</a>
</div>
</div>
<div class="col-md-10">
    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    <?php the_content(); ?>
</div>
</div>
<?php
$e++;
endwhile;

else:
    echo "<center>No Event Found!</center>";
endif;

// Reset Query
wp_reset_query();

?>


  </div>
  
  <div class="tab-pane fade list <?php echo (isset($_POST['action']) && $_POST['action'] == 'search_table') ? 'in active' : ''; ?>" id="venue-tables">
    <?php if ( is_user_logged_in() ) { ?>
        
        
        <form method="post">
            <?php wp_nonce_field( 'venue_'. $post->ID ); ?>
             <input type="hidden" name="action" value="search_table" />
             <label>Select Date</label>
            <input id="datepickerTable" type="text" name="date" value="<?php echo (isset($_POST['date'])) ? $_POST['date'] : date('m/d/Y'); ?>" />
            <input type="submit" value="Submit" />
        </form>
    <div class="row">    
<?php 
// In our file that handles the request, verify the nonce.
$nonce = $_REQUEST['_wpnonce'];
if ( wp_verify_nonce( $nonce, 'venue_' . $post->ID ) ) {

$tables = get_post_meta($post->ID, 'venue_tables', true);

$reservations = getTableReservations(NULL, $post->ID, $_POST['date'], 'OBJECT_K', 'table_number, user_id');


for($i=1; $i <= $tables; $i++) {

?>
<div class="col-md-2">
<?php if( isset( $reservations[$i] ) ) { ?>
        <p><button class="btn btn-danger btn-lg table" type="button"><?php echo $i; ?></button></p>
<?php } else { ?>   
<form method="post" action="<?php echo get_permalink( get_page_by_path( 'reserve-table' ) ); ?>">
    <?php wp_nonce_field( 'venue_'. $post->ID ); ?>
    <input type="hidden" name="action" value="reserve_table" />
    <input type="hidden" name="table_number" value="<?php echo $i; ?>" />
    <input type="hidden" name="post_id" value="<?php echo $post->ID; ?>" />
    <input type="hidden" name="date" value="<?php echo $_POST['date']; ?>" />      
    <p><button class="btn btn-success btn-lg table" type="submit"><?php echo $i; ?></button></p>
    </form>
<?php } ?>

</div>
<?php 
}
} ?>
      </div>  
<?php } else { ?>
    
        <center>
         <a href="<?php echo get_permalink( get_page_by_path( 'login' ) ); ?>" class="login-button"><span class="icon img-circle"><span class="glyphicon glyphicon-log-in"></span></span>Login</a>
                <a href="<?php echo get_permalink( get_page_by_path( 'register' ) ); ?>" class="register-button"><span class="icon img-circle"><span class="glyphicon glyphicon-pencil"></span></span>Register</a>  
        </center>
    <?php } ?>
  </div>
  
  
  <div class="tab-pane fade" id="venue-news">
    <?php

$e = 0;
// The Query
$query = new WP_Query( array(
    'post_type' => 'post',
    'posts_per_page' => 10,
    'meta_key' => 'post_venue',
    'meta_query' => array(
       array(
           'key' => 'post_venue',
           'value' => $post->ID,
           'compare' => 'IN',
       )
   )
) );

// The Loop
if ( $query->have_posts() ) :

while ( $query->have_posts() ) : $query->the_post();
?>
<div class="row <?php echo $even = (($e % 2) == 0) ? 'even' : 'odd'; ?>">
<div class="col-md-2">
<div class="thumbnail">
<a href="<?php the_permalink(); ?>">
<?php
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('thumbnail', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="img-responsive" style="width:100%">
                        <?php
                        }
?>
</a>
</div>
</div>
<div class="col-md-10">
    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    <?php the_excerpt(); ?>
</div>
</div>
<?php
$e++;
endwhile;

else:
    echo "<center>No News Found!</center>";
endif;

// Reset Query
wp_reset_query();

?>
  </div>
  
</div>

                
            </div>
            <div class="col-md-3">
                <?php get_sidebar('venue'); ?>
            </div>
        </div>
        </div>
      </div>

      
   </div>
   
<?php
        endwhile;
        endif;
?> 

<?php get_footer(); ?>
