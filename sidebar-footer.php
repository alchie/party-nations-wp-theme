<div id="footer-container">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                
                <div id="secondary">
	<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
	<div id="footer-1" class="footer widget-area" role="complementary">
		<?php dynamic_sidebar( 'footer-1' ); ?>
	</div><!-- #primary-sidebar -->
	<?php endif; ?>
</div><!-- #secondary -->

                </div>
                <div class="col-md-4">
                
              <div id="secondary">
	<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
	<div id="footer-2" class="footer widget-area" role="complementary">
		<?php dynamic_sidebar( 'footer-2' ); ?>
	</div><!-- #primary-sidebar -->
	<?php endif; ?>
</div><!-- #secondary -->
  
                </div>
                <div class="col-md-4">
             
             <div id="secondary">
	<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
	<div id="footer-3" class="footer widget-area" role="complementary">
		<?php dynamic_sidebar( 'footer-3' ); ?>
	</div><!-- #primary-sidebar -->
	<?php endif; ?>
</div><!-- #secondary -->   
                
                </div>                                
            </div>
        </div>
    </div>
