<?php
/* Template Name: Venues */
global $GLOBALS;
?>
<?php get_header(); ?>


<?php
		if( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();
?>

           
  <div id="page-container" class="single">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-title">
                    <h3>
                        <?php the_title(); ?>
                    </h3>
                </div>


            </div>
        </div>
      
         <div class="row">
            <div class="col-md-offset-3 col-md-6 main-content">
<?php

if( isset( $GLOBALS['table_reserved'] ) ) {
?>
<div class="alert alert-danger" style="margin-top:20px;">
 <center> <strong>Unable to process reservation!</strong><br> Table Number <?php echo $_POST['table_number']; ?> is already reserved!</center>
</div>
<?php
}

if( ! isset( $GLOBALS['table_reservation_id'] ) ) {
?>
<form method="post" style="margin-top:20px">
<?php wp_nonce_field( 'confirm_reservation_'. $_POST['post_id'] ); ?>
<input type="hidden" name="action" value="confirm_reservation" />           
<input type="hidden" name="post_id" value="<?php echo $_POST['post_id']; ?>" />       
<input type="hidden" name="date" value="<?php echo $_POST['date']; ?>" />
<input type="hidden" name="table_number" value="<?php echo $_POST['table_number']; ?>" />

<div class="col-md-12">
<div class="panel panel-default">
  <div class="panel-heading">Venue</div>
  <div class="panel-body">
    <?php $venue = get_post( $_POST['post_id'] ); echo $venue->post_title; ?>
  </div>
</div>
</div>

<div class="col-md-6">
<div class="panel panel-default">
  <div class="panel-heading">Table Number</div>
  <div class="panel-body">
    <?php echo $_POST['table_number']; ?>
  </div>
</div>
</div>

<div class="col-md-6">
<div class="panel panel-default">
  <div class="panel-heading">Date</div>
  <div class="panel-body">
    <?php echo $_POST['date']; ?>
  </div>
</div>
</div>

<p><input type="submit" class="btn btn-success btn-block btn-lg" value="Confirm Reservation" /></p>
</form>
<?php } else { ?>

<div class="well"><?php $venue = get_post( $_POST['post_id'] ); echo $venue->post_title; ?> Table Number: <?php echo $_POST['table_number']; ?> is now reserved under your name on <?php echo $_POST['date']; ?>! Thank you for the reservation!</div>

<?php } ?>                             
            </div>

        </div>
     
      </div>
      

      
      
      
   </div>
   
   <?php
				endwhile;
		endif;
			?> 

<?php get_footer(); ?>

