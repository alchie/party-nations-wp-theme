<?php get_header(); ?>

  <div id="venues-container" class="template">
  
    <div class="bg"></div>
     <div class="container">
       <div class="row">
        <div class="col-md-12 box-header">
            <h3 class="box-title">Venue</h3>
            <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam placerat mi vitae turpis porta bibendum vel ut nisi. </p>
        </div>
       </div> 
     </div>
     
<div class=" container">
    <div class="row">
        <div class="col-md-12">
            <ul class="pagination">
             
              <li class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>

            </ul>
        </div>
    </div>
</div>

<?php for($i=0; $i < 3; $i++) { ?>
    <div class="venue-list container">
      <div class="row">
        <div class="col-md-4">
 
            <div class="panel panel-default">
                 <div class="panel-thumbnail">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/venue_img1.jpg" class="" style="width:100%">
                 </div>
              <div class="panel-heading">
                <h3 class="panel-title">Panel title</h3>
              </div>
              <div class="panel-body">
                Panel content
              </div>
              <div class="panel-footer">
                    <a href="" class="btn btn-danger btn-block btn-venue">Read More</a>
              </div>
            </div>
        </div>
        
        <div class="col-md-4">
 
 
            <div class="panel panel-default">
            <div class="panel-thumbnail">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/venue_img2.jpg" class="" style="width:100%">
                 </div>
              <div class="panel-heading">
                <h3 class="panel-title">Panel title</h3>
              </div>
              <div class="panel-body">
                Panel content
              </div>
              <div class="panel-footer">
                <a href="" class="btn btn-danger btn-block btn-venue">Read More</a>
              </div>
            </div>
           
        </div>
        
        <div class="col-md-4">
 
 
            <div class="panel panel-default">
            <div class="panel-thumbnail">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/venue_img3.jpg" class="" style="width:100%">
                 </div>
              <div class="panel-heading">
                <h3 class="panel-title">Panel title</h3>
              </div>
              <div class="panel-body">
                Panel content
              </div>
              <div class="panel-footer">
                <a href="" class="btn btn-danger btn-block btn-venue">Read More</a>
              </div>
            </div>
           
        </div>
      </div>
    </div>
    <?php } ?>
    
    
    <div class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="pagination">
             
              <li class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>

            </ul>
        </div>
    </div>
</div>
    
 </div>
       
       
<?php get_template_part('footer', 'partners'); ?>

<?php get_footer(); ?>

