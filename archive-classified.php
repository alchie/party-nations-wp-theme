<?php
/* Template Name: Classifieds */
?>
<?php get_header(); ?>

  <div id="classifieds-container" class="template">
  
    <div class="bg"></div>
     <div class="container">
       <div class="row">
        <div class="col-md-12 box-header">
 <?php $promos = get_page( of_get_option('partynations_classified_page') ); ?>

 <h3 class="box-title"><?php echo $promos->post_title; ?></h3>
            <div class="text-center"><?php echo $promos->post_content; ?></div> 
            
             </div>
       </div> 
     </div>
     
<div class=" container">
    <div class="row">
        <div class="col-md-12">
        
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">Classifieds</li>
</ol>
 
<?php if( of_get_option('partynations_classified_category') ) { ?>
 <ul class="nav nav-tabs">
    <li><a href="?cat=">All</a></li>
 <?php $classcat = get_categories('hide_empty=0&child_of=' . of_get_option('partynations_classified_category') ); ?>
 <?php foreach($classcat as $cc) { ?>
  <li><a href="?cat=<?php echo $cc->term_id; ?>"><?php echo $cc->cat_name; ?></a></li>
<?php } ?>
  <ul>
<?php } ?>

        </div>
    </div>
</div>



    <div class="classified-list container">
      <div class="row">
      
<?php
$i=0;
//query_posts('post_per_page=9');
		if( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();
?>
        <div class="col-md-4">
 
            <div class="panel panel-default panel-<?php echo $i; ?>">
              <div class="panel-heading">
                <h3 class="panel-title"><?php the_title(); ?></h3>
                 <span class="arrow"></span>
              </div>
              <div class="panel-body">
                <?php echo substr(get_the_excerpt(), 0, 140); ?>
              </div>
              <div class="panel-footer">
                    Feb 2014
                    <a href="<?php the_permalink(); ?>" class="btn btn-danger btn-sm btn-item pull-right">Read More <i class="glyphicon glyphicon-circle-arrow-right"></i></a>
                    <div class="clearfix"></div>
              </div>
            </div>
        </div>
     
    <?php
   $i++;
				endwhile;
		endif;
	
/*	      
        
        <div class="col-md-4">
 

            <div class="panel panel-default panel-<?php echo $i; ?>-2">
            
              <div class="panel-heading">
                <h3 class="panel-title">Panel title</h3>
                <span class="arrow"></span>
              </div>
              <div class="panel-body">
                Panel content
              </div>
              <div class="panel-footer">
                Feb 2014
                <a href="" class="btn btn-danger btn-sm btn-item pull-right">Read More <i class="glyphicon glyphicon-circle-arrow-right"></i></a>
                <div class="clearfix"></div>
              </div>
            </div>
           
        </div>
        
        <div class="col-md-4">
 
 
            <div class="panel panel-default panel-<?php echo $i; ?>-3">

              <div class="panel-heading">
                <h3 class="panel-title">Panel title</h3>
                 <span class="arrow"></span>
              </div>
              <div class="panel-body">
                Panel content
              </div>
              <div class="panel-footer">
              Feb 2014
                <a href="" class="btn btn-danger btn-sm btn-item pull-right">Read More <i class="glyphicon glyphicon-circle-arrow-right"></i></a>
                <div class="clearfix"></div>
              </div>
              
            </div>
           
        </div>
        
        */
        
        ?>
      </div>
    </div>
   

    
    
    <div class="container">
    <div class="row">
        <div class="col-md-12">
        
        
       <a href="#" class="btn btn-default btn-sm btn-block">Show More <i class="glyphicon glyphicon-circle-arrow-down"></i></a>
            
        </div>
    </div>
</div>
    
 </div>
       
       
<?php get_template_part('footer', 'partners'); ?>

<?php get_footer(); ?>

