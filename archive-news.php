<?php
/* Template Name: News */
?>
<?php get_header(); ?>

  <div id="news-container" class="template">
  
    <div class="bg"></div>
     <div class="container">
       <div class="row">
        <div class="col-md-12 box-header">
            <h3 class="box-title">News</h3>
            <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam placerat mi vitae turpis porta bibendum vel ut nisi. </p>
        </div>
       </div> 
     </div>
     
     
<div class="container">
    <div class="row">
        <div class="col-md-12">
        
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">News</li>
</ol>

            <ul class="pagination pull-right">
             
              <li class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>

            </ul>
            
<div class="filter">
    
</div>
        </div>
    </div>
</div>


    <div class="news-list container">
    <?php for($i=0; $i < 5; $i++) { ?>
      <div class="row">
        <div class="col-md-12">
 
            <div class="item">
                <div class="row">
                    <div class="col-md-3 news-thumbnail">
                       <img src="<?php echo get_template_directory_uri(); ?>/images/venue_img1.jpg" class="" style="width:100%">
                    </div>
                    <div class="col-md-9 details">
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <span class="meta">Posted by <a href="">Brian Kaur</a> - February 16, 2014</span>
                            <h3 class="panel-title">Panel title</h3>
                          </div>
                          <div class="panel-body">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam placerat mi vitae turpis porta bibendum vel ut nisi.
                          </div>
                          <div class="panel-footer">
                            <span class="comments">22 Comments</span> <a href="#" class="btn btn-danger btn-sm">Read More <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
                          </div>
                        </div>                    
                    </div>
                    
                </div>
            </div>
            
        </div>
      </div>
          <?php } ?>
    </div>

    
    
    <div class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="pagination">
             
              <li class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>

            </ul>
        </div>
    </div>
</div>
    
 </div>
       
       
<?php get_template_part('footer', 'partners'); ?>

<?php get_footer(); ?>

