<?php




function my_login_stylesheet() { ?>
    <style>
    <!--
    .login h1 a {
        background-image: none,url(<?php echo get_template_directory_uri(); ?>/images/newlogo.png);
        background-size: 475px 196px;
        height: 196px;
        width:475px;
        margin-left:-78px;
    }
    -->
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

add_action('wp_logout', 'redirect_logout_to_front_page' );
function redirect_logout_to_front_page() {
            wp_redirect( home_url() );
            exit();
}
        
function partynations_theme_setup() {
	
    if ( ! function_exists( 'optionsframework_init' ) ) {
		define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
		require_once dirname( __FILE__ ) . '/inc/options_framework/options-framework.php';
	}
    require('inc/bootstrap3-menu-walker.php');
    require('inc/table-reservation.php');
    //require('inc/events-widgets.php');
     //require('inc/promotions-widgets.php');
	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// Switches default core markup for search form, comment form, and comments
	// to output valid HTML5.
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

	// This theme uses wp_nav_menu() in one location.
    register_nav_menu( 'header-primary', __( 'Primary Nav', 'partynations_theme' ) );
	register_nav_menu( 'header-mobile', __( 'Mobile Nav', 'partynations_theme' ) );
    register_nav_menu( 'footer', __( 'Footer Links', 'partynations_theme' ) );

	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 604, 270, true );
	
}
add_action( 'after_setup_theme', 'partynations_theme_setup' );


/******************************************************************

        Add Scripts and Styles

*******************************************************************/
function partynations_theme_scripts_styles() {
	
	
	// Bootstrap 3
	//wp_enqueue_script( 'bootstrap3-js', '//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js', array( 'jquery' ), '2014-03-03', true );
	wp_enqueue_script( 'bootstrap3-js', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '2014-03-03', true );
	//wp_enqueue_style( 'bootstrap3-css', '//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css', array(), null );
    //wp_enqueue_style( 'bootstrap3-css', get_template_directory_uri() . "/css/bootstrap.min.css", array(), null );
    
    // Bootswatch
    //wp_enqueue_style( 'bootstrap3-css', 'http://bootswatch.com/yeti/bootstrap.min.css', array(), null );
    wp_enqueue_style( 'bootstrap3-css', get_template_directory_uri() . "/css/yeti.css", array(), null );
    
    // Font Awesome
    //wp_enqueue_style( 'font-awesome', '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css', array(), null );
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . "/css/font-awesome/css/font-awesome.min.css", array(), null );
     
	//wp_enqueue_style( 'theme-style', get_stylesheet_uri(), array(), '2013-07-18' );
	wp_enqueue_style( 'default-css', get_template_directory_uri() . "/css/default.css", array(), '2014-03-03' );
	wp_enqueue_style( 'custom-css', get_template_directory_uri() . "/css/custom.css", array('default-css'), '2014-03-03' );
	wp_enqueue_style( 'custom-responsive', get_template_directory_uri() . "/css/responsive.css", array('default-css', 'custom-css'), '2014-03-03' );
	
	wp_enqueue_script( 'partynations.custom.js', get_template_directory_uri() . '/js/partynations.custom.js', array( 'jquery' ), '2014-03-03', true );

}
add_action( 'wp_enqueue_scripts', 'partynations_theme_scripts_styles' );

function partynations_theme_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Main Sidebar', 'partynations_theme' ),
		'id'            => 'sidebar-main',
		'description'   => __( 'Main Sidebar', 'partynations_theme' ),
		'before_widget' => '<aside id="%1$s" class="block-inner widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3><em></em></div>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Venue Sidebar', 'partynations_theme' ),
		'id'            => 'sidebar-venue',
		'description'   => __( 'Venue Sidebar', 'partynations_theme' ),
		'before_widget' => '<aside id="%1$s" class="block-inner widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3><em></em></div>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'News Sidebar', 'partynations_theme' ),
		'id'            => 'sidebar-news',
		'description'   => __( 'News Sidebar', 'partynations_theme' ),
		'before_widget' => '<aside id="%1$s" class="block-inner widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3><em></em></div>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Event Sidebar', 'partynations_theme' ),
		'id'            => 'sidebar-event',
		'description'   => __( 'event Sidebar', 'partynations_theme' ),
		'before_widget' => '<aside id="%1$s" class="block-inner widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3><em></em></div>',
	) );
	
	
    register_sidebar( array(
		'name'          => __( 'Promotions Sidebar', 'partynations_theme' ),
		'id'            => 'sidebar-promo',
		'description'   => __( 'Promotions Sidebar', 'partynations_theme' ),
		'before_widget' => '<aside id="%1$s" class="block-inner widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3><em></em></div>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer 1', 'partynations_theme' ),
		'id'            => 'footer-1',
		'description'   => __( 'Footer 1', 'partynations_theme' ),
		'before_widget' => '<aside id="%1$s" class="block-inner widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3><em></em></div>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer 2', 'partynations_theme' ),
		'id'            => 'footer-2',
		'description'   => __( 'Footer 2', 'partynations_theme' ),
		'before_widget' => '<aside id="%1$s" class="block-inner widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3><em></em></div>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer 3', 'partynations_theme' ),
		'id'            => 'footer-3',
		'description'   => __( 'Footer 3', 'partynations_theme' ),
		'before_widget' => '<aside id="%1$s" class="block-inner widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3><em></em></div>',
	) );
	
	
}
add_action( 'widgets_init', 'partynations_theme_widgets_init' );

/*=======================================================================*/
/*==================== RCK Modification Follwos =========================*/
/*=======================================================================*/
//Ref: http://code.tutsplus.com/tutorials/reusable-custom-meta-boxes-part-3-extra-fields--wp-23821

$custom_meta_boxes = array();

// enqueue scripts and styles, but only if is_admin
if(is_admin()) {
	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_script('jquery-ui-slider');
//	wp_enqueue_script('jquery-ui-sortable');
	wp_enqueue_script('custom-js', get_template_directory_uri().'/js/custom-js.js', array('jquery-ui-sortable') );
	wp_enqueue_style('jquery-ui-custom', get_template_directory_uri().'/css/jquery-ui-timepicker-addon.css');
	wp_enqueue_script('DateTimePicker-js', get_template_directory_uri().'/js/jquery-ui-timepicker-addon.js', array('jquery-ui-datepicker'));	
	wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.css');
} else {
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.css');
}

// add some custom js to the head of the page
  add_action('admin_head','add_custom_scripts');
  function add_custom_scripts() {
	global $custom_meta_boxes, $post;
	
	$output = '<script type="text/javascript">
				jQuery(function() {';
				
	foreach($custom_meta_boxes as $box){
		if( $box['post_type'] == $post->post_type ){
			$custom_meta_fields = $box['fields'];
									
			foreach ($custom_meta_fields as $field) { // loop through the fields looking for certain types
				// date
				if($field['type'] == 'date')
					$output .= 'jQuery(".datepicker").datepicker();';
				// datetime
				if($field['type'] == 'datetime')
					$output .= 'jQuery(".datetimepicker").datetimepicker();';
				// slider
				if ($field['type'] == 'slider') {
					$value = get_post_meta($post->ID, $field['id'], true);
					if ($value == '') $value = $field['min'];
					$output .= '
							jQuery( "#'.$field['id'].'-slider" ).slider({
								value: '.$value.',
								min: '.$field['min'].',
								max: '.$field['max'].',
								step: '.$field['step'].',
								slide: function( event, ui ) {
									jQuery( "#'.$field['id'].'" ).val( ui.value );
								}
							});';
				}
			}			
		}
	}
	$output .= '});
		</script>';
		
	echo $output;
}


// The Callback
function show_custom_meta_box($post, $meta_box) {
	
	global $custom_meta_boxes;	
	$custom_meta_fields = $custom_meta_boxes[ $meta_box['id'] ]['fields'];
	
	

	// Use nonce for verification
	echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
	
	// Begin the field table and loop
	echo '<table class="form-table">';
	foreach ($custom_meta_fields as $field) {
		// get value of this field if it exists for this post
		$meta = get_post_meta($post->ID, $field['id'], true);
		// begin a table row with
		echo '<tr>
				<th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
				<td>';
				switch($field['type']) {
					// text
					case 'text':
						echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// textarea
					case 'textarea':
						echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea>
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// checkbox
					case 'checkbox':
						echo '<input type="checkbox" name="'.$field['id'].'" id="'.$field['id'].'" ',$meta ? ' checked="checked"' : '',' />
								<label for="'.$field['id'].'">'.$field['desc'].'</label>';
					break;
					// select
					case 'select':
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';

						foreach ($field['options'] as $option) {
							echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';
						}
						echo '</select><br /><span class="description">'.$field['desc'].'</span>';
					break;
					// radio
					case 'radio':
						foreach ( $field['options'] as $option ) {
							echo '<input type="radio" name="'.$field['id'].'" id="'.$option['value'].'" value="'.$option['value'].'" ',$meta == $option['value'] ? ' checked="checked"' : '',' />
									<label for="'.$option['value'].'">'.$option['label'].'</label><br />';
						}
						echo '<span class="description">'.$field['desc'].'</span>';
					break;
					// checkbox_group
					case 'checkbox_group':
						foreach ($field['options'] as $option) {
							echo '<input type="checkbox" value="'.$option['value'].'" name="'.$field['id'].'[]" id="'.$option['value'].'"',$meta && in_array($option['value'], $meta) ? ' checked="checked"' : '',' /> 
									<label for="'.$option['value'].'">'.$option['label'].'</label><br />';
						}
						echo '<span class="description">'.$field['desc'].'</span>';
					break;
					// tax_select
					case 'tax_select':
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'">
								<option value="">Select One</option>'; // Select One
						$terms = get_terms($field['id'], 'get=all');
						$selected = wp_get_object_terms($post->ID, $field['id']);
						foreach ($terms as $term) {
							if (!empty($selected) && !strcmp($term->slug, $selected[0]->slug)) 
								echo '<option value="'.$term->slug.'" selected="selected">'.$term->name.'</option>'; 
							else
								echo '<option value="'.$term->slug.'">'.$term->name.'</option>'; 
						}
						$taxonomy = get_taxonomy($field['id']);
						echo '</select><br /><span class="description"><a href="'.get_bloginfo('home').'/wp-admin/edit-tags.php?taxonomy='.$field['id'].'">Manage '.$taxonomy->label.'</a></span>';
					break;
					// post_list
					case 'post_list':
					$items = get_posts( array (
						'post_type'	=> $field['post_type'],
						'posts_per_page' => -1
					));
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'">
								<option value="">Select One</option>'; // Select One
							foreach($items as $item) {
								echo '<option value="'.$item->ID.'"',$meta == $item->ID ? ' selected="selected"' : '','>'.$item->post_type.': '.$item->post_title.'</option>';
							} // end foreach
						echo '</select><br /><span class="description">'.$field['desc'].'</span>';
					break;
					// date
					case 'date':
						echo '<input type="text" class="datepicker" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// date
					case 'datetime':
						echo '<input type="text" class="datetimepicker" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// slider
					case 'slider':
					$value = $meta != '' ? $meta : '0';
						echo '<div id="'.$field['id'].'-slider"></div>
								<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$value.'" size="5" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// image
					case 'image':
						$image = get_template_directory_uri().'/images/image.png';	
						echo '<span class="custom_default_image" style="display:none">'.$image.'</span>';
						if ($meta) { $image = wp_get_attachment_image_src($meta, 'medium');	$image = $image[0]; }				
						echo	'<input name="'.$field['id'].'" type="hidden" class="custom_upload_image" value="'.$meta.'" />
									<img src="'.$image.'" class="custom_preview_image" alt="" /><br />
										<input class="custom_upload_image_button button" type="button" value="Choose Image" />
										<small>&nbsp;<a href="#" class="custom_clear_image_button">Remove Image</a></small>
										<br clear="all" /><span class="description">'.$field['desc'].'</span>';
					break;
					// repeatable
					case 'repeatable':
						echo '<a class="repeatable-add button" href="#">+</a>
								<ul id="'.$field['id'].'-repeatable" class="custom_repeatable">';
						$i = 0;
						if ($meta) {
							foreach($meta as $row) {
								echo '<li><span class="sort hndle">|||</span>
											<input type="text" name="'.$field['id'].'['.$i.']" id="'.$field['id'].'" value="'.$row.'" size="30" />
											<a class="repeatable-remove button" href="#">-</a></li>';
								$i++;
							}
						} else {
							echo '<li><span class="sort hndle">|||</span>
										<input type="text" name="'.$field['id'].'['.$i.']" id="'.$field['id'].'" value="" size="30" />
										<a class="repeatable-remove button" href="#">-</a></li>';
						}
						echo '</ul>
							<span class="description">'.$field['desc'].'</span>';
					break;
				} //end switch
		echo '</td></tr>';
	} // end foreach
	echo '</table>'; // end table
}



/************************************************************************/
/* ----------------- Create Custom Post type for News ----------------- */
/************************************************************************/
add_action('init', 'news_register');
 
function news_register() {
 
	$labels = array(
		'name' => _x('News', 'post type general name'),
		'singular_name' => _x('News', 'post type singular name'),
		'add_new' => _x('Add New', 'news item'),
		'add_new_item' => __('Add New News Item'),
		'edit_item' => __('Edit News Item'),
		'new_item' => __('New News Item'),
		'view_item' => __('View News Item'),
		'search_items' => __('Search News'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		//'menu_icon' => get_stylesheet_directory_uri() . '/article16.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments'),
		'taxonomies' => array('category'),
		'has_archive' => true
	  ); 
 
	register_post_type( 'news' , $args );
}
register_taxonomy_for_object_type( 'category', 'news' );

/************************************************************************/
/************************************************************************/


/************************************************************************/
/* ----------------- Create Custom Post type for Venue ----------------- */
/************************************************************************/
add_action('init', 'venue_register');
 
function venue_register() {
 
	$labels = array(
		'name' => _x('Venues', 'post type general name'),
		'singular_name' => _x('Venue', 'post type singular name'),
		'add_new' => _x('Add New', 'venues item'),
		'add_new_item' => __('Add New Venue Item'),
		'edit_item' => __('Edit Venue Item'),
		'new_item' => __('New Venue Item'),
		'view_item' => __('View Veue Item'),
		'search_items' => __('Search Venues'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		//'menu_icon' => get_stylesheet_directory_uri() . '/article16.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ), //, 'comments'
		'has_archive' => true
	  ); 
 
	register_post_type( 'venue' , $args );
	
	$args_property_type = array(
		    'hierarchical'          => true, 
		    'labels'                => array(
	            'name'                       => _x( 'Locations', 'taxonomy general name' ),
				'singular_name'              => _x( 'Location', 'taxonomy singular name' ),
				'search_items'               => __( 'Search  location' ),
				'popular_items'              => __( 'Popular  locations' ),
				'all_items'                  => __( 'All  locations' ),
				'parent_item'                => null,
				'parent_item_colon'          => null,
				'edit_item'                  => __( 'Edit  location' ),
				'update_item'                => __( 'Update  location' ),
				'add_new_item'               => __( 'Add New  location' ),
				'new_item_name'              => __( 'New location  Name' ),
				'separate_items_with_commas' => __( 'Separate location with commas' ),
				'add_or_remove_items'        => __( 'Add or remove  location' ),
				'choose_from_most_used'      => __( 'Choose from the most used  location' ),
				'not_found'                  => __( 'No location found' ),
				'menu_name'                  => __( 'Locations' ),
					),
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'location' ),
	);

	register_taxonomy( 'location', 'venue', $args_property_type );
	
	/*
	
	$args_property_type = array(
		    'hierarchical'          => true, 
		    'labels'                => array(
	            'name'                       => _x( 'Ratings', 'taxonomy general name' ),
				'singular_name'              => _x( 'Rating', 'taxonomy singular name' ),
				'search_items'               => __( 'Search  rating' ),
				'popular_items'              => __( 'Popular  ratings' ),
				'all_items'                  => __( 'All  ratings' ),
				'parent_item'                => null,
				'parent_item_colon'          => null,
				'edit_item'                  => __( 'Edit  rating' ),
				'update_item'                => __( 'Update  rating' ),
				'add_new_item'               => __( 'Add New  rating' ),
				'new_item_name'              => __( 'New rating  Name' ),
				'separate_items_with_commas' => __( 'Separate rating with commas' ),
				'add_or_remove_items'        => __( 'Add or remove  rating' ),
				'choose_from_most_used'      => __( 'Choose from the most used  rating' ),
				'not_found'                  => __( 'No rating found' ),
				'menu_name'                  => __( 'Ratings' ),
					),
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'rating' ),
	);
	
	register_taxonomy( 'rating', 'venue', $args_property_type );
	*/
}


$custom_meta_boxes['mbox_venue_contact'] = array(
    'id' => 'mbox_venue_contact',  
    'title' => 'Other Info',
    'context' => 'normal',
    'priority' => 'high',
	'post_type' => 'venue',
    'fields' => array(
        array(
            'label' => 'Website', 
            'desc' => 'Website Address',
            'id' => 'venue_website',
            'type' => 'text',
            'default' => ''
        ),
        array(
            'label' => 'Contact',
            'desc' => 'Contacct Phone Number',
            'id' => 'venue_phone',
            'type' => 'text',
            'default' => ''
        ),
        array(
            'label' => 'Email',
            'desc' => 'Email Address',
            'id' => 'venue_email',
            'type' => 'text',
            'default' => ''
        ),
        
        array(
			'label'	=> 'Number of Tables',
			'desc'	=> 'Number of Tables',
			'id'	=> 'venue_tables',
			'type'	=> 'text',
			 'default' => ''
		), 
		

         array(
            'label' => 'Opening Hours',
            'desc' => 'Opening Hours',
            'id' => 'venue_opening',
            'type' => 'text',
            'default' => ''
        ),
         array(
            'label' => 'Happy Hours',
            'desc' => 'Happy Hours',
            'id' => 'venue_happy',
            'type' => 'text',
            'default' => ''
        ),
    )
);

/************************************************************************/
/************************************************************************/


/************************************************************************/
/* ----------------- Create Custom Post type for Events --------------- */
/************************************************************************/
add_action('init', 'events_register');
 
function events_register() {
 
	$labels = array(
		'name' => _x('Events', 'post type general name'),
		'singular_name' => _x('Event', 'post type singular name'),
		'add_new' => _x('Add New', 'event item'),
		'add_new_item' => __('Add New Event Item'),
		'edit_item' => __('Edit Event Item'),
		'new_item' => __('New Event Item'),
		'view_item' => __('View Event Item'),
		'search_items' => __('Search Venues'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		//'menu_icon' => get_stylesheet_directory_uri() . '/article16.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ), //, 'comments'
		'has_archive' => true
	  ); 
 
	register_post_type( 'event' , $args );
}

$venues_options = array();
$query = new WP_Query(array(
                'post_type' => 'venue',
                'orderby' => 'title',
                'order' => 'ASC',
                'posts_per_page' => -1
            ));
$venues_options[] = array('value' => '', 'label' => '-- select venue --');
while ($query->have_posts()) {
	$query->the_post();
	$title = get_the_title();
	if (empty($title)) continue;
	$venues_options[] = array('value' => get_the_ID(), 'label' => $title);
}

	wp_reset_query();
		
$custom_meta_boxes['mbox_event_info'] = array(
    'id' => 'mbox_event_info',  
    'title' => 'Event Info',
    'context' => 'normal',
    'priority' => 'high',
	'post_type' => 'event',
    'fields' => array(
		array(
			'label' => 'Venue', 
            'desc' => 'Event Venue',
            'id' => 'event_venue',
			'type'	=> 'select',
			'options' => $venues_options
		),
        array(
            'label' => 'Age Limit',
            'desc' => '',
            'id' => 'event_age_limit',
            'type' => 'text',
            'default' => ''
        ),
		
		
		array(
			'label'	=> 'Date & Time',
			'desc'	=> 'Date & Time of Event',
			'id'	=> 'event_datetime',
			'type'	=> 'datetime'
		),
		
				
		
    )
);

add_action('init', 'promotion_register');
 
function promotion_register() {
 
	$labels = array(
		'name' => _x('Promotions', 'post type general name'),
		'singular_name' => _x('Promotion', 'post type singular name'),
		'add_new' => _x('Add New', 'news item'),
		'add_new_item' => __('Add New Promotion Item'),
		'edit_item' => __('Edit Promotion Item'),
		'new_item' => __('New Promotion Item'),
		'view_item' => __('View Promotion Item'),
		'search_items' => __('Search Promotion'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		//'menu_icon' => get_stylesheet_directory_uri() . '/article16.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ), //, 'comments'
		//'taxonomies' => array('category'),
		'has_archive' => true
	  ); 
 
	register_post_type( 'promotion' , $args );
}

$custom_meta_boxes['mbox_promo_info'] = array(
    'id' => 'mbox_promo_info',  
    'title' => 'Promo Info',
    'context' => 'normal',
    'priority' => 'high',
	'post_type' => 'promotion',
    'fields' => array(
        array(
            'label' => 'Header Title',
            'desc' => '',
            'id' => 'promo_header_title',
            'type' => 'text',
            'default' => ''
        ),
		array(
			'label' => 'Venue', 
            'desc' => 'Promo Venue',
            'id' => 'promo_venue',
			'type'	=> 'select',
			'options' => $venues_options
		),
        array(
            'label' => 'Website',
            'desc' => '',
            'id' => 'promo_website',
            'type' => 'text',
            'default' => ''
        ),
        
		array(
			'label'	=> 'Date & Time',
			'desc'	=> 'Date & Time of Promotion',
			'id'	=> 'promo_datetime',
			'type'	=> 'datetime'
		)	,
		array(
            'label' => 'Sub-Title',
            'desc' => '',
            'id' => 'promo_subtitle',
            'type' => 'text',
            'default' => ''
        ),	
    )
);


add_action('init', 'classified_register');
 
function classified_register() {
 
	$labels = array(
		'name' => _x('Classifieds', 'post type general name'),
		'singular_name' => _x('Classified', 'post type singular name'),
		'add_new' => _x('Add New', 'news item'),
		'add_new_item' => __('Add New Classified Item'),
		'edit_item' => __('Edit Classified Item'),
		'new_item' => __('New Classified Item'),
		'view_item' => __('View Classified Item'),
		'search_items' => __('Search Classified'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		//'menu_icon' => get_stylesheet_directory_uri() . '/article16.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ), //, 'comments'
		//'taxonomies' => array('category'),
		'has_archive' => true,
		
	  ); 
 
	register_post_type( 'classified' , $args );
	register_taxonomy_for_object_type( 'category', 'classified'  );
}
/************************************************************************/

add_action('init', 'partner_register');
 
function partner_register() {
 
	$labels = array(
		'name' => _x('Partners', 'post type general name'),
		'singular_name' => _x('Partner', 'post type singular name'),
		'add_new' => _x('Add New', 'news item'),
		'add_new_item' => __('Add New Item'),
		'edit_item' => __('Edit Item'),
		'new_item' => __('New Item'),
		'view_item' => __('View Item'),
		'search_items' => __('Search'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => false,
		'publicly_queryable' => false,
		'show_ui' => true,
		'query_var' => false,
		//'menu_icon' => get_stylesheet_directory_uri() . '/article16.png',
		'rewrite' => false,
		'capability_type' => 'page',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array( 'title', 'thumbnail', 'excerpt' ), //, 'comments'
		//'taxonomies' => array('category'),
		'has_archive' => false,
		
	  ); 
 
	register_post_type( 'partner' , $args );
}

/************************************************************************/

add_action('init', 'service_register');
 
function service_register() {
 
	$labels = array(
		'name' => _x('Services', 'post type general name'),
		'singular_name' => _x('Service', 'post type singular name'),
		'add_new' => _x('Add New', 'news item'),
		'add_new_item' => __('Add New Item'),
		'edit_item' => __('Edit Item'),
		'new_item' => __('New Item'),
		'view_item' => __('View Item'),
		'search_items' => __('Search'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => false,
		'publicly_queryable' => false,
		'show_ui' => true,
		'query_var' => false,
		//'menu_icon' => get_stylesheet_directory_uri() . '/article16.png',
		'rewrite' => false,
		'capability_type' => 'page',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array( 'title', 'thumbnail', 'excerpt', 'page-attributes' ), //, 'comments'
		//'taxonomies' => array('category'),
		'has_archive' => false,
		
	  ); 
 
	register_post_type( 'service' , $args );
}

/************************************************************************/
$custom_meta_boxes['post_settings'] = array(
    'id' => 'post_settings',  
    'title' => 'Settings',
    'context' => 'normal',
    'priority' => 'high',
    'post_type' => 'post',
    'fields' => array(
        array(
            'label' => 'Hide Partners Section',
            'desc' => '',
            'id' => 'common_settings_hide_partners',
            'type' => 'checkbox',
            'default' => '1',
        ),
        
        array(
			'label' => 'Venue', 
            'desc' => 'Venue',
            'id' => 'post_venue',
			'type'	=> 'select',
			'options' => $venues_options
		),
		
    )
);

$custom_meta_boxes['page_settings'] = array(
    'id' => 'page_settings',  
    'title' => 'Settings',
    'context' => 'normal',
    'priority' => 'high',
    'post_type' => 'page',
    'fields' => array(
        array(
            'label' => 'Hide Partners Section',
            'desc' => '',
            'id' => 'common_settings_hide_partners',
            'type' => 'checkbox',
            'default' => '1',
        ),
		
    )
);

$custom_meta_boxes['venue_settings'] = array(
    'id' => 'venue_settings',  
    'title' => 'Settings',
    'context' => 'normal',
    'priority' => 'high',
    'post_type' => 'venue',
    'fields' => array(
        array(
            'label' => 'Hide Partners Section',
            'desc' => '',
            'id' => 'common_settings_hide_partners',
            'type' => 'checkbox',
            'default' => '1',
        ),
		
    )
);
$custom_meta_boxes['event_settings'] = array(
    'id' => 'event_settings',  
    'title' => 'Settings',
    'context' => 'normal',
    'priority' => 'high',
    'post_type' => 'event',
    'fields' => array(
        array(
            'label' => 'Hide Partners Section',
            'desc' => '',
            'id' => 'common_settings_hide_partners',
            'type' => 'checkbox',
            'default' => '1',
        ),
		
    )
);
$custom_meta_boxes['news_settings'] = array(
    'id' => 'news_settings',  
    'title' => 'Settings',
    'context' => 'normal',
    'priority' => 'high',
    'post_type' => 'news',
    'fields' => array(
        array(
            'label' => 'Hide Partners Section',
            'desc' => '',
            'id' => 'common_settings_hide_partners',
            'type' => 'checkbox',
            'default' => '1',
        ),
		
    )
);
$custom_meta_boxes['promo_settings'] = array(
    'id' => 'promo_settings',  
    'title' => 'Settings',
    'context' => 'normal',
    'priority' => 'high',
    'post_type' => 'promotion',
    'fields' => array(
        array(
            'label' => 'Hide Partners Section',
            'desc' => '',
            'id' => 'common_settings_hide_partners',
            'type' => 'checkbox',
            'default' => '1'
        ),
		
    )
);
$custom_meta_boxes['classified_settings'] = array(
    'id' => 'classified_settings',  
    'title' => 'Settings',
    'context' => 'normal',
    'priority' => 'high',
    'post_type' => 'classified',
    'fields' => array(
        array(
            'label' => 'Hide Partners Section',
            'desc' => '',
            'id' => 'common_settings_hide_partners',
            'type' => 'checkbox',
            'default' => '1',
        ),
		
    )
);
//=======================================================================
// Common Funcions Follows
//=======================================================================

// Add the Meta Box
function add_custom_meta_boxes() {
	global $custom_meta_boxes;
	
	foreach($custom_meta_boxes as $box) {
        add_meta_box($box['id'], $box['title'], 'show_custom_meta_box', $box['post_type'], $box['context'], $box['priority']);
    }
}
add_action('add_meta_boxes', 'add_custom_meta_boxes');


add_action('save_post', 'save_custom_meta');
// Save the Data
function save_custom_meta($post_id) {
	global $custom_meta_boxes, $post;
		
	// verify nonce
	if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__))) 
		return $post_id;
		
	// check autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return $post_id;
		
	// check permissions
	if ('page' == $_POST['post_type']) {
		if (!current_user_can('edit_page', $post_id)) {
			return $post_id;
		} elseif (!current_user_can('edit_post', $post_id)) {
			return $post_id; 
			}
	}
	
	
	// loop through fields and save the data
	foreach($custom_meta_boxes as $box){
	
		if( $box['post_type'] == $post->post_type ){
			$custom_meta_fields = $box['fields'];
			
			foreach ($custom_meta_fields as $field) {
				if($field['type'] == 'tax_select') continue;

				$old = get_post_meta($post_id, $field['id'], true);
				
				$new = $_POST[$field['id']];
				if ($new && $new != $old) {
					update_post_meta($post_id, $field['id'], $new);
				} elseif ('' == $new && $old) {
					delete_post_meta($post_id, $field['id'], $old);
				}
			}
		}
	}

	 // enf foreach
	
	// save taxonomies
	$post = get_post($post_id);
	$category = $_POST['category'];
	wp_set_object_terms( $post_id, $category, 'category' );
}

function eventDateCountdown($datetime, $d='') {

    $date1 = strtotime($datetime);
    $date2 = time();
    $subTime = $date1 - $date2;
    return array(
        'years' => ( ($subTime/(60*60*24*365)) ),
        'days' => ( ($subTime/(60*60*24))%365 ),
        'hours' => ( ($subTime/(60*60))%24 ),
        'mins' => ( ($subTime/60)%60 )
    );
    
}

function eventAdjacentFilter() {
    global $wpdb, $post;
    return 'INNER JOIN ' . $wpdb->prefix.'postmeta ON ('.$wpdb->prefix.'posts.ID = '.$wpdb->prefix.'postmeta.post_id)';
}

function eventPrevPost() {
    //add_filter( "get_previous_post_join", 'eventAdjacentFilter' );
    //add_filter( "get_previous_post_where",  'filter_previous_post_where');
    //add_filter( "get_previous_post_sort",  'filter_previous_post_where');
    return get_adjacent_post(false, '', 1);
}

function eventNextPost() {
    //add_filter( "get_next_post_join",  'filter_previous_post_where');
    //add_filter( "get_next_post_where",  'filter_previous_post_where');
    //add_filter( "get_next_post_sort",  'filter_previous_post_where');
    return get_adjacent_post(false, '', 0);
}


function partynations_venue_rating_calculate($id) {
    $comment = get_comment($id);
	global $wpdb;
    $rating = $wpdb->get_var(
			$wpdb->prepare("SELECT AVG(comment_karma) FROM $wpdb->comments WHERE ". 
				"comment_post_ID = %d AND comment_karma > %d AND comment_approved = %d", 
				$comment->comment_post_ID, 0, 1)); 		
    
    add_post_meta( $comment->comment_post_ID, '_venue_rating', round( $rating ), true);
}

add_action( 'wp_insert_comment', 'partynations_venue_rating_calculate');

function get_user_role() {
	global $current_user;

	$user_roles = $current_user->roles;
	$user_role = array_shift($user_roles);

	return $user_role;
}

add_action('after_setup_theme', 'disable_admin_toolbar' );
function disable_admin_toolbar()
        {
            get_currentuserinfo();
            global $user_level;
            if (( $user_level < 7 ) && !is_admin() ) {
                 show_admin_bar(false);
            }
        }

add_action( 'admin_init', 'disable_access_adminpanel' );
function disable_access_adminpanel() {
            get_currentuserinfo();
            global $user_level;
            if ( ( $user_level < 7 ) && is_admin() && (strpos($_SERVER['PHP_SELF'], 'admin-ajax.php') == 0)) {
                 wp_redirect( home_url() );
                 exit;
            }
        }
        
function wp_ajax_head() {
echo "\n<script type=\"text/javascript\">
<!--
var ajax_url = \"".admin_url( "admin-ajax.php" )."\";
-->
</script>\n";
}
add_action('wp_head', 'wp_ajax_head');


add_action( 'wp_ajax_update_venue', 'partynations_ajax_update_venue' );
function partynations_ajax_update_venue() {
    
    if ( current_user_can('edit_post', $_POST['id']) &&  wp_verify_nonce( $_POST['nonce'], 'manage_venue_' . $_POST['id'] )) {
        //echo json_encode($_POST);
        wp_update_post( array(
            'ID' => $_POST['id'],
            'post_content' => $_POST['description'],
            'post_title' => $_POST['title']
        ) );
        wp_set_post_terms( $_POST['id'], $_POST['location'],'location', false );
        update_post_meta($_POST['id'], 'venue_opening', $_POST['opening']);
        update_post_meta($_POST['id'], 'venue_happy', $_POST['happy']);
        update_post_meta($_POST['id'], 'venue_website', $_POST['website']);
        update_post_meta($_POST['id'], 'venue_phone', $_POST['phone']);
        update_post_meta($_POST['id'], 'venue_tables', $_POST['tables']);
        die("1");
    }
}

add_action( 'wp_ajax_update_venue_image', 'partynations_update_venue_image' );
function partynations_update_venue_image() {
    set_post_thumbnail($_POST['id'], media_handle_upload('file', $_POST['id'])); 
    header("location: /my-account");
    exit;
}

