<?php     
get_header(); 

?>

           
  <div id="venue-single-container" class="single">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-title">
                    <h3>
<?php if( get_user_role() == 'author') { ?>
Merchant Account
<?php } else { ?>
My Account
<?php } ?>
                    </h3>
                </div>


            </div>
        </div>


         <div class="row">
            <div class="col-md-9 main-content" style="margin-top:20px;">
<?php if( get_user_role() == 'subscriber') { ?>
<?php get_template_part('my-account', 'regular'); ?>
<?php } elseif( get_user_role() == 'author') { ?>
<?php get_template_part('my-account', 'merchant'); ?>
<?php } ?>
        </div>
        
           <div class="sidebar col-md-3">
                <?php get_sidebar(); ?>
            </div>
        
        </div>
      </div>
      
      
   </div>
   


<?php get_footer(); ?>

