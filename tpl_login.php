<?php
/* Template Name: Login Page */
?>
<?php get_header(); ?>

<hr >
<div class="container" style="margin-top:50px;">
<div class="row">
    <div class="col-md-offset-4 col-md-4">
    
      <form name="loginform" id="loginform" class="login-container" action="<?php bloginfo('home'); ?>/wp-login.php" method="post">
       
  <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title"><?php the_title(); ?></h3>
  </div>
  <div class="panel-body">
   
   

  <div class="form-group">
    <label for="username">Username</label>
    <input type="text" name="log" class="form-control" id="username" placeholder="Enter username">
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" name="pwd" class="form-control" id="password" placeholder="Password">
  </div>

 <input type="hidden" name="redirect_to" value="<?php bloginfo('home'); ?>" />
		    <input type="hidden" name="testcookie" value="1" />
		    
   
  </div>
   <div class="panel-footer"><button type="submit" class="btn btn-success btn-sm btn-block">Login</button></div>
</div>
</form>
<a href="<?php echo get_permalink( get_page_by_path( 'forgotpassword' ) ); ?>" class="pull-right">Forgot Password?</a>
<a href="<?php echo get_permalink( get_page_by_path( 'register' ) ); ?>">Create New Account</a> 
</div>
</div>
</div>
<?php get_footer(); ?>
