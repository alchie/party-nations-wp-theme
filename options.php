<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = wp_get_theme();
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'options_framework_theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {
	
	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}
	
	// Pull all the post categories into an array
	$options_post_categories = array();
	$options_post_categories_obj = get_categories('hide_empty=0');
	$options_post_categories[''] = 'Select a category:';
	foreach ($options_post_categories_obj as $cat) {
		$options_post_categories[$cat->term_id] = $cat->name;
	}

    $pages_all = array();
    $pages = get_pages(); 
    foreach ( $pages as $page ) {
        $pages_all[$page->ID] = $page->post_title;
    }
  
    $promotions_all = array();
    $promotions_q = new WP_Query( array(
    'post_type' => 'promotion',
    'posts_per_page' => -1 ) );

if ( $promotions_q->have_posts() ) {

while ( $promotions_q->have_posts() ) {
    $promotions_q->the_post();
    $promotions_all[get_the_ID()] = get_the_title();
}

}
wp_reset_query();

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/images/';

	$options = array();

/*
	$options[] = array(
		'name' => __('Front Page Sliders', 'options_framework_theme'),
		'type' => 'heading');
	
	$options[] = array(
		'name' => __('# of Slides', 'options_framework_theme'),
		'desc' => __('Number of Slides to display.', 'options_framework_theme'),
		'id' => 'partynations_default_fp_slides_count',
		'type' => 'select',
		'options' => array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) );
	
	$slider_count = (of_get_option('partynations_default_fp_slides_count') + 1);
	
	for($sc=0; $sc<$slider_count;$sc++) {
		$num = $sc+1;
		$options[] = array(
			'name' => __('Slider '.$num.' Image', 'options_framework_theme'),
			'desc' => __('Slider '.$num.' Image.', 'options_framework_theme'),
			'id' => 'partynations_frontpage_slider'.$num.'_img',
			'type' => 'upload');
		
		$options[] = array(
			'name' => __('Slider '.$num.' Link', 'options_framework_theme'),
			'desc' => __('Slider '.$num.' Link.', 'options_framework_theme'),
			'id' => 'partynations_frontpage_slider'.$num.'_link',
			'std' => '',
			'type' => 'text');
	}
*/
     $options[] = array(
		'name' => __('Front Page', 'options_framework_theme'),
		'type' => 'heading');
		
	$options[] = array(
		'name' => __('Partners Section', 'options_framework_theme'),
		'desc' => __('show / hide partner section on front page.', 'options_framework_theme'),
		'id' => 'partynations_frontpage_partner_section',
		'type' => 'checkbox',
		'std' => 'true',
		);
		
    $options[] = array(
		'name' => __('Merchant Settings', 'options_framework_theme'),
		'type' => 'heading');
		
    
     $options[] = array(
		'name' => __('Merchant Event Limit', 'options_framework_theme'),
		'desc' => __('Merchant Event Limit.', 'options_framework_theme'),
		'id' => 'partynations_user_event_limit',
		'type' => 'text',
		'std' => '1',
		);
	
	$options[] = array(
		'name' => __('Merchant Promotion Limit', 'options_framework_theme'),
		'desc' => __('Merchant Promotion Limit.', 'options_framework_theme'),
		'id' => 'partynations_user_promotion_limit',
		'type' => 'text',
		'std' => '1',
		);
	
	$options[] = array(
		'name' => __('Merchant Classified Limit', 'options_framework_theme'),
		'desc' => __('Merchant Classified Limit.', 'options_framework_theme'),
		'id' => 'partynations_user_classified_limit',
		'type' => 'text',
		'std' => '1',
		);
		
    $options[] = array(
		'name' => __('Top Navigation', 'options_framework_theme'),
		'type' => 'heading');
		
    $options[] = array(
		'name' => __('Featured Promotion', 'options_framework_theme'),
		'desc' => __('Featured promotion.', 'options_framework_theme'),
		'id' => 'partynations_promo',
		'type' => 'select',
		'options' => $promotions_all );
    
     $options[] = array(
		'name' => __('Promotions Page', 'options_framework_theme'),
		'desc' => __('promotions page.', 'options_framework_theme'),
		'id' => 'partynations_promotions_page',
		'type' => 'select',
		'options' => $pages_all );
	
	 $options[] = array(
		'name' => __('Classifieds Page', 'options_framework_theme'),
		'desc' => __('Classifieds page.', 'options_framework_theme'),
		'id' => 'partynations_classified_page',
		'type' => 'select',
		'options' => $pages_all );

	 $options[] = array(
		'name' => __('Classifieds Parent Category', 'options_framework_theme'),
		'desc' => __('Classifieds Parent Category.', 'options_framework_theme'),
		'id' => 'partynations_classified_category',
		'type' => 'select',
		'options' => $options_post_categories );
				
	$options[] = array(
		'name' => __('Social Links', 'options_framework_theme'),
		'type' => 'heading');

	$options[] = array(
		'name' => __('Facebook', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'partynations_social_facebook',
		'std' => '#facebook',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Twitter', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'partynations_social_twitter',
		'std' => '#twitter',
		'type' => 'text');

	
	$options[] = array(
		'name' => __('Google Plus', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'partynations_social_googleplus',
		'std' => '#google-plus',
		'type' => 'text');

	return $options;
}
