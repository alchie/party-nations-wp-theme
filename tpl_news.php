<?php
/* Template Name: News */
?>
<?php get_header(); ?>

  <div id="news-container" class="template">
  

   <?php
	global $post;
	 $envira = new Envira_Gallery_Lite; 
    $gallery = $envira->get_gallery($post->ID);
    
    if( isset( $gallery['gallery'] ) && ( count( $gallery['gallery'] ) > 0) ) { 
    
  ?>
  
 <div id="bg-slider-container" class="carousel slide bg-header" data-ride="carousel">
  <div class="container">
        <ol class="carousel-indicators">
        <?php $n=0; foreach( $gallery['gallery'] as $id => $item ) { ?>
          <li data-target="#bg-slider-container" data-slide-to="<?php echo $n; ?>" <?php echo ($n==0) ? 'class="active"' : ''; ?>></li>
         <?php $n++; } ?>
        </ol>
        <div class="carousel-inner">
        <?php $n=0; foreach( $gallery['gallery'] as $id => $item ) { ?>
          <div class="item <?php echo ($n==0) ? 'active' : ''; ?>">
            <img src="<?php echo $item['src']; ?>" id="<?php echo $id; ?>" title="<?php echo $item['title']; ?>" alt="<?php echo $item['alt']; ?>">
          </div>
        <?php $n++; } ?>
        </div>
        <a class="left carousel-control" href="#bg-slider-container" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#bg-slider-container" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
</div>
      </div>
   <?php } else { ?>
     <div class="bg"></div>
   <?php } ?>
   
     <div class="container">
       <div class="row">
        <div class="col-md-12 box-header">
         <?php
		if( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();
?>

 <h3 class="box-title"><?php the_title(); ?></h3>
            <div class="text-center"><?php the_content(); ?></div>
            
<?php
				endwhile;
		endif;
			?>
           
        </div>
       </div> 
     </div>
     
     
<div class="container">
    <div class="row">
        <div class="col-md-12">
        
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">News</li>
</ol>


            

        </div>
    </div>
</div>

    <div class="news-list container">
      <div class="row">
        <div class="col-md-9">
         <form method="get">
  <div class="filter pull-right">
    <span>Filter by Month</span> 
    <select class="input-sm" name="month">
    <option value="1" <?php if( date('m') == 1) echo 'SELECTED'; ?>>January</option>
    <option value="2" <?php if( date('m') == 2) echo 'SELECTED'; ?>>February</option>
    <option value="3" <?php if( date('m') == 3) echo 'SELECTED'; ?>>March</option>
    <option value="4" <?php if( date('m') == 4) echo 'SELECTED'; ?>>April</option>
    <option value="5" <?php if( date('m') == 5) echo 'SELECTED'; ?>>May</option>
    <option value="6" <?php if( date('m') == 6) echo 'SELECTED'; ?>>June</option>
    <option value="7" <?php if( date('m') == 7) echo 'SELECTED'; ?>>July</option>
    <option value="8" <?php if( date('m') == 8) echo 'SELECTED'; ?>>August</option>
    <option value="9" <?php if( date('m') == 9) echo 'SELECTED'; ?>>September</option>
    <option value="10" <?php if( date('m') == 10) echo 'SELECTED'; ?>>October</option>
    <option value="11" <?php if( date('m') == 11) echo 'SELECTED'; ?>>November</option>
    <option value="12" <?php if( date('m') == 12) echo 'SELECTED'; ?>>December</option>
    </select>
    
    <button type="submit" class="btn btn-danger btn-xs">Search</button>
</div>
</form>

<div class="clearfix"></div>
        
<?php
$args = array(
    'post_type' => 'news',
    'posts_per_page' => 5,
    'paged' => max( 1, get_query_var('paged') ) 
) ;
if( isset($_GET['month']) ) {
    $args['monthnum'] = $_GET['month'];
}
// The Query
query_posts( $args );

// The Loop
if ( have_posts() ) :

while ( have_posts() ) : the_post();

?>


 
            <div class="item">
            
                <div class="row">
                
                    <div class="col-md-3 news-thumbnail">
                       
                          <?php
                       
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('medium', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:100%">
                        <?php
                        }
     ?>
 
                    </div>
                    <div class="col-md-9 details">
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <p class="meta"><strong>Posted By</strong> <a href="<?php echo get_the_author_meta('user_url', $post->post_author);?>"><?php echo get_the_author_meta('display_name', $post->post_author);?></a> - <?php the_date('M d, Y'); ?>
                            <h3 class="panel-title"><?php the_title(); ?></h3>
                          </div>
                          <div class="panel-body">

<?php the_content(); ?>

                          </div>
                          <div class="panel-footer">
                            <span class="comments"> <?php echo wp_count_comments( get_the_ID() )->approved; ?> Comments</span> <a href="<?php the_permalink(); ?>" class="btn btn-danger btn-sm">Read More <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
                          </div>
                        </div>                    
                    </div>
                    
                </div>
                <hr />
            </div>
            
     

<?php
endwhile;

?>


    <div class="row">
        <div class="col-md-12">
        
            <ul class="pagination">
             
              <?php
 
 global $wp_query;

$big = 999999999; // need an unlikely integer

$pagination = paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $wp_query->max_num_pages,
	'type' => 'array'
) );
if( $pagination ) {
foreach($pagination as $page) {
    echo "<li>" . $page ."</li>";
}
}
?>
             
              

            </ul>
        </div>
    </div>

       <!-- end pagination -->
    

 <?php

endif;
// Reset Query
wp_reset_query();

?>


   </div>
   
     <div class="col-md-3">
                <?php get_sidebar('news'); ?>
            </div>
   
      </div>
    </div>

    
    

       
<?php get_template_part('footer', 'partners'); ?>

</div>
<?php get_footer(); ?>

