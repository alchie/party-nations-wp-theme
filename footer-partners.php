<?php

global $post;
echo $hide = get_post_meta($post->ID, 'common_settings_hide_partners', true);

if( $hide != 'on' ) :

$n=4;
$c=0;
query_posts( array(
    'post_type' => 'partner',
    'posts_per_page' => -1,
) );

		if( have_posts() ) :
?>	
<div id="partners-container"><div class="container">
      <div class="row"><div class="col-md-12">
            <h3 class="box-title">Our Partners</h3>

            <div class="row">
            
<div id="carousel-partners" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="item active">
<?php while ( have_posts() ) : the_post(); ?>
       
       <?php
                       
                       if ( has_post_thumbnail() ) {
                            echo "<div class='col-md-3'>";
	                        the_post_thumbnail('medium', array('class' => 'img-responsive') );
	                        echo "</div>";
                        } 
                        ?>
     
<?php
  $n--;
    $c++;
    if( ($n == 0) && ($c != 4)) {
    $n=4;
    $c=0;
    echo '</div>
          <div class="item">
          ';
    }
   
endwhile; ?>
				
				</div>
        </div>
        <a class="left carousel-control" href="#carousel-partners" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-partners" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>
				

            </div>
            
        </div></div>    
    </div></div>

<?php
		endif;
		
		endif;
		?>    
