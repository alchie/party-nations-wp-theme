<?php
// Creating the widget 
class Events_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
        // Base ID of your widget
        'Events_widget', 

        // Widget name will appear in UI
        __('Events List', 'events_widget_domain'), 

        // Widget description
        array( 'description' => __( 'Events List', 'events_widget_domain' ), ) 
        );
    }

    // Creating widget front-end
    // This is where the action happens
    public function widget( $args, $instance ) {
        global $post;
        $title = apply_filters( 'widget_title', $instance['title'] );
        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if ( ! empty( $title ) )
        echo $args['before_title'] . $title . $args['after_title'];

        // This is where you run the code and display the output
        //echo __( 'Hello, World!', 'events_widget_domain' );
        

// The Query
$query = new WP_Query( array(
    'post_type' => 'event',
    'posts_per_page' => 5,
    'meta_key' => 'event_venue',
    'meta_query' => array(
       array(
           'key' => 'event_venue',
           'value' => $post->ID,
           'compare' => 'IN',
       )
   )
) );

// The Loop
if ( $query->have_posts() ) :

while ( $query->have_posts() ) : $query->the_post();
?>
<div class="thumbnail">
<a href="<?php the_permalink(); ?>">
<?php
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('thumbnail', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="img-responsive" style="width:100%">
                        <?php
                        }
?>
</a></div>
<?php
endwhile;

endif;

// Reset Query
wp_reset_query();
        
        echo $args['after_widget'];
    }
		
    // Widget Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'Events', 'events_widget_domain' );
        }
        // Widget admin form
        ?>
        <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php 
    }
	
    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
} // Class wpb_widget ends here

// Register and load the widget
function events_load_widget() {
	register_widget( 'Events_widget' );
}
add_action( 'widgets_init', 'events_load_widget' );
