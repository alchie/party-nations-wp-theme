<?php


    
        add_action('init', 'addTableReservation');
        add_action( 'load-themes.php', 'createTableReservation' );
    
    
    function createTableReservation()
    {
        global $wpdb;
        $table = $wpdb->prefix . "venue_table_reservation";
        $sql = "CREATE TABLE `{$table}` (
         `id` bigint(20) NOT NULL AUTO_INCREMENT,
         `user_id` bigint(20) NOT NULL,
         `post_id` bigint(20) NOT NULL,
         `table_number` int(10) NOT NULL,
         `sched_date` date NOT NULL,
         `date_reserved` datetime NOT NULL,
         PRIMARY KEY (`id`)
        ) ;";
          
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );   
    }
    
    
    function addTableReservation() {
        global $GLOBALS;
        $nonce = $_REQUEST['_wpnonce'];
        if ( wp_verify_nonce( $nonce, 'confirm_reservation_' . $_POST['post_id'] ) ) {
            global $wpdb;
            global $current_user;
            get_currentuserinfo();
            $results = $wpdb->get_results( sprintf("SELECT * FROM `%svenue_table_reservation` WHERE `user_id` = '%d' AND `post_id` = '%d' AND `table_number` = '%d' AND `sched_date` = '%s' LIMIT 1", $wpdb->prefix, $current_user->ID, $_POST['post_id'], $_POST['table_number'], date( 'Y-m-d', strtotime($_POST['date']))) );
            
            if( ! isset($results[0]) ) {
                $wpdb->query( sprintf("INSERT INTO `%svenue_table_reservation` (`id`, `user_id`, `post_id`, `table_number`, `sched_date`, `date_reserved`) VALUES (NULL, '%d', '%d', '%d', '%s', '%s');", $wpdb->prefix, $current_user->ID, $_POST['post_id'], $_POST['table_number'], date( 'Y-m-d', strtotime($_POST['date'])), date('Y-m-d H:i:s')) );
                $GLOBALS['table_reservation_id'] = $wpdb->insert_id;
            } else {
                $GLOBALS['table_reserved'] = true;
            }
        }
    }
    
    function getTableReservations($user_id=NULL, $post_id=NULL, $date=NULL, $output='OBJECT', $fields='*') {
            global $wpdb;
            
            $sql = sprintf("SELECT %s FROM `%svenue_table_reservation`", $fields, $wpdb->prefix );
            
            $where = array();
            
            if($user_id != NULL) {
                $where[] = sprintf("`user_id` = '%d'", $user_id);
            }
            
            if($post_id != NULL) {
                $where[] = sprintf("`post_id` = '%d'", $post_id);
            }
            
            if($post_id != NULL) {
                $where[] = sprintf( "`sched_date` = '%s'", date( 'Y-m-d', strtotime($date) ) );
            }
            
            if( count($where) > 0 ) {
                $sql .= "WHERE " . implode(' AND ', $where);
            }
            
            $sql .= " ORDER BY `sched_date` ASC";
            $results = $wpdb->get_results( $sql, $output );
            
            return $results;
    }
    


