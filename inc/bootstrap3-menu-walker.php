<?php
// Bootstrap Menu Walker
class Bootstrap3_Walker_Nav_Menu extends Walker_Nav_Menu {
        
        
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"sub-menu dropdown-menu\">\n";
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
                if ($item->current) {
                   $classes[] = 'active';
                }
		$classes[] = 'menu-item-' . $item->ID;
                $classes[] = 'menu-item-' . $item->post_name;
		
		$class_names = apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args );
		$dropdown = false;
		if( in_array('menu-item-has-children', $class_names) ) {
			$classes[] = 'dropdown';
			$class_names = apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args );
			$dropdown = true;
		}
		
		$class_names = join( ' ', $class_names );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $value . $class_names .'>';

		$atts = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
		$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
		$atts['href']   = ! empty( $item->url )        ? $item->url        : '';

		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

		$attributes = '';
                
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}
		
		$item_output = $args->before;
		$item_output .= '<a'. $attributes;
		if($dropdown) {
			$item_output .= ' class="dropdown-toggle" data-toggle="dropdown" ';
		}
		$item_output .= '>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}


}