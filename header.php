<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head profile="http://www.w3.org/2005/10/profile">
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" />
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=485703541441571";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="wrap">
    
    <div id="sticky-nav-container">
         <div class="container">
            <div class="row">
                <div class="col-md-12">
                
                
<nav id="sticky-navigation" class="sticky navigation main-navigation" role="navigation">
     <a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php wp_nav_menu( array(
    'theme_location'    => 'header-primary',
    'container'     => 'nav',
    'container_id'      => 'header-primary',
    'container_class'   => 'navbar navbar-default hidden-xs visible-sm visible-md visible-lg',
    'menu_class'        => 'nav navbar-nav', 
    'echo'          => true,
    'items_wrap'        => '<div class="collapse navbar-collapse navbar-ex1-collapse"><ul id="%1$s" class="%2$s">%3$s</ul></div>',
    'depth'         => 10, 
    'walker'        => new Bootstrap3_Walker_Nav_Menu
) );
 ?>
</div>
</nav><!-- #site-navigation -->
    
                
                </div>
            </div>
        </div>
    </div>
    
    <div id="top-container">
        <div class="container">
            <div class="row">
                <div class="col-md-5">

<?php
// The Query
$headlines = new WP_Query( array(
    'post_type' => 'news',
    'posts_per_page' => 5 
) );

// The Loop
if ( $headlines->have_posts() ) :

?>

                
                     <div class="headlines pull-left">
                        <div class="headline-label pull-left"><img class="img-circle " src="<?php echo get_template_directory_uri(); ?>/images/headline.png"> Headlines</div>
                         <div class="headline-content pull-left">
                              <div id="carousel-headlines" class="carousel slide" data-ride="carousel">
                                
                                <div class="carousel-inner">
                                
<?php 
$n=0;
while ( $headlines->have_posts() ) : $headlines->the_post(); ?>
<div class="item <?php if ($n==0) { echo 'active'; } ?>"><?php the_title(); ?></div>
<?php 
$n++;
endwhile; 
?>
                                
                                  
                                </div>
                                <!--<a class="left carousel-control" href="#carousel-headlines" data-slide="prev">
                                  <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>-->
                                <a class="right carousel-control" href="#carousel-headlines" data-slide="next">
                                  <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                              </div>
                         </div>
                         
                     </div>

<?php 
endif;
wp_reset_postdata();
?>
                </div>
                <div class="col-md-7 right">
                     <div class="right-section pull-right">
                     <iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.partynations.com%2F&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21&amp;appId=445371815498377" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px; float:left;width: 100px;" allowTransparency="true"></iframe>
                     

<?php if ( is_user_logged_in() ) {  ?>
    <a href="<?php echo wp_logout_url(); ?>" class="register-button pull-right"><span class="icon img-circle"><span class="glyphicon glyphicon-log-out"></span></span>Logout</a> 
    <a href="<?php echo get_permalink( get_page_by_path( 'my-account' ) ); ?>" class="register-button pull-right"><span class="icon img-circle"><span class="glyphicon glyphicon-list"></span></span>My Account</a> 
<?php } else { ?>
                <a href="#" class="facebook"><span class="icon img-circle facebook"></span> Sign in with facebook</a>
                <a href="<?php echo get_permalink( get_page_by_path( 'login' ) ); ?>" class="login-button"><span class="icon img-circle"><span class="glyphicon glyphicon-log-in"></span></span>Login</a>
                <a href="<?php echo get_permalink( get_page_by_path( 'register' ) ); ?>" class="register-button"><span class="icon img-circle"><span class="glyphicon glyphicon-pencil"></span></span>Register</a>                

<?php } ?>

                     </div>
                </div>
            </div>
        </div>
    </div>
    <div id="header-container"><div class="container">
    <div class="row">
        <div class="col-md-12">
               <ul class="social pull-right">
                    <li><a href="<?php echo of_get_option('partynations_social_googleplus'); ?>" class="google">Google+</a></li>
                    <li><a href="<?php echo of_get_option('partynations_social_facebook'); ?>" class="facebook">Facebook</a></li>
                    <li><a href="<?php echo of_get_option('partynations_social_twitter'); ?>" class="twitter">Twitter</a></li>                                        
               </ul>
              <div id="branding">
                <h1 id="site-title"><a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                 <?php bloginfo( 'name' ); ?></a></h1>
                 <h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
                
             </div>
            
            <div class="row">
                <div class="col-md-12">
             <nav id="site-navigation" class="navigation main-navigation" role="navigation">
     <a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>

            <?php wp_nav_menu( array(
    'theme_location'    => 'header-primary',
    'container'     => 'nav',
    'container_id'      => 'header-primary',
    'container_class'   => 'navbar navbar-default hidden-xs visible-sm visible-md visible-lg',
    'menu_class'        => 'nav navbar-nav', 
    'echo'          => true,
    'items_wrap'        => '<div class="collapse navbar-collapse navbar-ex1-collapse"><ul id="%1$s" class="%2$s">%3$s<li class="search-form">
                <form method="get" id="form-search-form">
                        <input type="text" name="s" value="" class="form-control search-place" id="search-place-input" placeholder="Search Place"/>
                        <input type="submit" value="Search" id="search-place-submit" /> 
                     </form>
                      <a href="javascript:void(0)" id="search-place-button">Search</a>
                      </li>
                      </ul>
    </div>',
    'depth'         => 10, 
    'walker'        => new Bootstrap3_Walker_Nav_Menu
) );
 ?>

    </nav><!-- #site-navigation -->
    
    
                        
        </div>
             
             <!--<div class="col-md-2 account-buttons">
                <a href="#login" class="login-button"><span class="icon img-circle"><span class="glyphicon glyphicon-log-in"></span></span>Login</a>
                <a href="#register" class="register-button"><span class="icon img-circle"><span class="glyphicon glyphicon-pencil"></span></span>Register</a>                
             </div>-->
             
        </div>
        </div>
    </div>
    </div></div>
