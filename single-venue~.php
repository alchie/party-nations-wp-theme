<?php
/* Template Name: Venues */
?>
<?php get_header(); ?>


<?php
		if( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();
?>

           
  <div id="venue-single-container" class="single">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-title">
                    <h3>
                        Venue
                    </h3>
                </div>
                
                

                <div class="prev-next-container">
                    <div class="prev col-md-6">
                    <a href="#">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <h4>PREV PLACE</h4>
                        <p>Place Title</p>
                    </a>
                    </div>
                    <div class="next col-md-6">
                    <a href="#">
                        <span class="glyphicon glyphicon-chevron-right"></span>      
                        <h4>NEXT PLACE</h4>
                        <p>Place Title</p>              
                    </a>
                    </div>
                    <div class="clearfix"></div>
                </div>                
                

                
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="/venues/">Venue</a></li>
  <li class="active"><?php the_title(); ?></li>
</ol>


            </div>
        </div>
        <div class="post">

         <div class="row">
            <div class="col-md-8">
            
                <div class="venue-details">
                    <h3 class="venue-title"><?php the_title(); ?></h3>
                        <div class="thumbnail">
                             <?php
                       
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('medium', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:100%">
                        <?php
                        }
     ?>
                        </div>
                     
                     
                </div>
                
       
      
       
       <a href="" class="btn btn-danger btn-sm pull-right">Reserve a Table</a>
        <?php get_template_part('social', 'media'); ?>
       <br class="clearfix">
<hr />
<div class="venue-meta">
<p><span class="label">Location:</span> <?php echo get_post_meta(get_the_ID(), 'venue_location', true); ?></p>
                            <p><span class="label">Opening Hours:</span> <?php echo get_post_meta(get_the_ID(), 'venue_opening', true); ?></p>
                            <p><span class="label">Happy Hours:</span> <?php echo get_post_meta(get_the_ID(), 'venue_happy', true); ?></p>   
                            <p><span class="label">Website:</span> <a href="<?php echo get_post_meta(get_the_ID(), 'venue_website', true); ?>"><?php echo get_post_meta(get_the_ID(), 'venue_website', true); ?></a></p>     
                            <p><span class="label">Phone Number:</span> <?php echo get_post_meta(get_the_ID(), 'venue_phone', true); ?></p> 
                             <p><span class="label">Rating:</span>  <?php the_rating(); ?></p> 
                              
</div>         
<?php the_content(); ?>



<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="750" data-numposts="5" data-colorscheme="light"></div>
                
            </div>
            <div class="col-md-4">
                <?php get_sidebar('venue'); ?>
            </div>
        </div>
        </div>
      </div>
      

      
      
      
   </div>
   
   <?php
				endwhile;
		endif;
			?> 

<?php get_footer(); ?>

