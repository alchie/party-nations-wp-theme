<?php
/* Template Name: Register Page */
?>
<?php get_header(); ?>
<hr >
 <div class="container" style="margin-top:50px;">
<div class="row">
    <div class="col-md-offset-4 col-md-4">
    
          <form name="loginform" id="loginform" class="login-container" action="<?php echo wp_registration_url(); ?>" method="post">
       
  <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title"><?php the_title(); ?></h3>
  </div>
  <div class="panel-body">
   
   

  <div class="form-group">
    <label for="user_login">Username</label>
    <input name="user_login" type="text" class="form-control" id="user_login" placeholder="Enter desired username">
  </div>
  <div class="form-group">
    <label for="user_email">Password</label>
    <input name="user_email" type="email" class="form-control" id="user_email" placeholder="Enter email address">
  </div>
    
    A password will be e-mailed to you.
   
  </div>
   <div class="panel-footer"><button type="submit" class="btn btn-success btn-sm btn-block">Register Now</button></div>
</div>
</form>
<a href="<?php echo get_permalink( get_page_by_path( 'login' ) ); ?>">Login to existing account</a>
</div>
</div>
</div>


<?php get_footer(); ?>
