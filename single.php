<?php get_header(); ?>

  <div id="page-container" class="single">
  <div class="container">
  
   <div class="row">
            <div class="col-md-12">
                <div class="main-title">
                    <h3>
                       <?php the_title(); ?>
                    </h3>
                </div>


            </div>
        </div>
        
      <div class="row">
        <div class="col-md-8 main-content">

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content" style="margin-top:20px;">
		<?php
			the_content();
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfourteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );

			edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
           
			
        </div>
        <div class="col-md-4 sidebar">
           <?php get_sidebar(); ?>
        </div></div>
    </div></div>
    


<?php get_footer(); ?>
