<?php get_header(); ?>

  <div id="page-container" class="single">
  <div class="container">
  
   <div class="row">
            <div class="col-md-12">
                <div class="main-title">
                    <h3>
                       
                    </h3>
                </div>


            </div>
        </div>
        
      <div class="row">
        <div class="col-md-8 main-content">
           
           <?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					get_template_part( 'content', get_post_format() );

				endwhile;
			?>
			
        </div>
        <div class="col-md-4 sidebar">
           <?php get_sidebar(); ?>
        </div></div>
    </div></div>
    


<?php get_footer(); ?>
