<?php get_header(); ?>

    <div id="slider-container">
    <div class="container">
    <div class="row">
    <div class="col-md-12">
    
     <div id="carousel-slider-container" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carousel-slider-container" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-slider-container" data-slide-to="1"></li>
          <li data-target="#carousel-slider-container" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="item active">
            <img src="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" alt="First slide">
          </div>
          <div class="item">
            <img src="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" alt="First slide">
          </div>
          <div class="item">
            <img src="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" alt="First slide">
          </div>
        </div>
        <a class="left carousel-control" href="#carousel-slider-container" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-slider-container" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>
      
    </div>    
    </div>
    </div>
    </div>
    
   <?php get_template_part('front', 'services'); ?>
    
    
    <div id="main-container"><div class="container">
      <div class="row">
        <div class="col-md-8 main-content">
           <div class="widget-area">
            <div class="widget-header">
                <h3 class="widget-title">Lastest News</h3>
            </div>
            <ul class="latest-news">
			<?php
			$args = array(
				'post_type' => 'news',
				'orderby' => 'modified',
				'posts_per_page' => 5				
			);
			// the query
			$the_query = new WP_Query( $args );
			if( $the_query->have_posts() ){
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					
					/* $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array(175, 166) );
					if( $thumb['0'] != ''){
						$url = $thumb['0'];
					} else {
						$url = get_template_directory_uri() . "/images/no-image-available.jpg";
					} */
			?>
			<li>
				<div class="row">
					<div class="col-md-3">
					<div class="thumbnail">
						<?php
                       
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('medium', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:100%">
                        <?php
                        }
     ?>
     </div>
				   </div>
				   <div class="col-md-9">
						<p class="meta"><strong>Posted By</strong> <a href="<?php echo get_the_author_meta('user_url', $post->post_author);?>"><?php echo get_the_author_meta('display_name', $post->post_author);?></a> - <?php the_date('M d, Y'); ?>
						<h3 class="title"><?php the_title(); ?></h3>
						<p class="excerpt"><?php the_excerpt(); ?></p>
						<span class="comments"><?php echo $post->comment_count?> Comments</span>
						<a href="<?php the_permalink(); ?>" class="btn btn-danger btn-sm pull-right">Read More...</a>
				   </div>
				</div>
			</li>
			<?php
				}
			}
			wp_reset_postdata();
			?>
            </ul>
           </div>
        </div>
        <div class="col-md-4 sidebar">
           <?php get_sidebar(); ?>
        </div></div>
    </div></div>
    
    
    <div id="upcomingevent-container"><div class="container">
    <div class="row"><div class="col-md-12">
            <h3 class="box-title">Upcoming Event</h3>
            
            <div id="carousel-upcomingevent" class="carousel slide" data-ride="carousel">
       <!-- <ol class="carousel-indicators">
          <li data-target="#carousel-upcomingevent" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-upcomingevent" data-slide-to="1"></li>
          <li data-target="#carousel-upcomingevent" data-slide-to="2"></li>
        </ol> -->
        <div class="carousel-inner">
          <div class="item active">

<?php
$n=2;
$c=0;
// The Query
query_posts( array(
    'post_type' => 'event',
    'posts_per_page' => 6
) );

// The Loop
if ( have_posts() ) :

while ( have_posts() ) : the_post();


?>
            <div class="col-md-6">
            <div class="item-box">
            <div class="thumbnail pull-left">
                <?php
                       
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('medium', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:100%">
                        <?php
                        }
     ?>
              </div>
            <div class="details pull-left">
                <h3><?php the_title(); ?></h3>
                 <p><span class="label">Venue:</span> <?php if( get_post_meta(get_the_ID(), 'event_venue', true) ) { ?><a href="<?php echo get_permalink( get_post_meta(get_the_ID(), 'event_venue', true) ); ?>"><?php echo get_the_title( get_post_meta(get_the_ID(), 'event_venue', true) ); ?></a><?php } ?></p>
                            <p><span class="label">City:</span> <?php echo get_post_meta(get_the_ID(), 'event_city', true); ?></p>
                            <p><span class="label">Age Limit:</span> <?php echo get_post_meta(get_the_ID(), 'event_age_limit', true); ?></p>   
                            <p><span class="label">Services:</span> <?php echo get_post_meta(get_the_ID(), 'event_services', true); ?></p>     
                            <p><span class="label">Date &amp; Time:</span> <?php echo get_post_meta(get_the_ID(), 'event_datetime', true); ?></p> 
            </div>
            <div class="clearfix"></div>
            </div>
            </div>
            
<?php

$n--;
$c++;
 if( ($n == 0) && ( ($c % 2) == 0 ) && ( $c != 6 ) ) {
    $n=2;
    echo '</div>
          <div class="item">
          ';
 }
endwhile;
endif;
wp_reset_query();
?>

            </div>
        </div>
        <a class="left carousel-control" href="#carousel-upcomingevent" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-upcomingevent" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>
            
            
        </div></div>
    </div></div>
    
    <div id="venue-container"><div class="container">
    <div class="row"><div class="col-md-12">
            <h3 class="box-title">Venue</h3>
            
<?php

// The Query
query_posts( array(
    'post_type' => 'venue',
    'posts_per_page' => 3,
    'paged' => max( 1, get_query_var('paged') )
) );

// The Loop
if ( have_posts() ) :

while ( have_posts() ) : the_post();

?>
            <div class="col-md-4">
                <center>
                <h3><?php the_title(); ?></h3>
                <div class="thumbnail">
                <?php
                       
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('medium', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:100%">
                        <?php
                        }
     ?>
              </div>
                <p><?php the_excerpt(); ?></p>
                               </center>
            </div>

<?php 
endwhile;
endif;
wp_reset_query();
?>
           
            
            
        </div></div>
    </div></div>
    
    
<?php  if(  of_get_option('partynations_frontpage_partner_section') ) { ?>
    <?php get_template_part('footer', 'partners'); ?>
<?php } ?>
    


<?php get_footer(); ?>
