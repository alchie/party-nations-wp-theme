<?php
/* Template Name: Promotions */
?>
<?php get_header(); ?>

  <div id="promotions-container" class="template">
  
      <?php
	global $post;
	 $envira = new Envira_Gallery_Lite; 
    $gallery = $envira->get_gallery($post->ID);
    
    if( isset( $gallery['gallery'] ) && ( count( $gallery['gallery'] ) > 1) ) { 
    
  ?>
  
 <div id="bg-slider-container" class="carousel slide bg-header" data-ride="carousel">
  <div class="container">
        <ol class="carousel-indicators">
        <?php $n=0; foreach( $gallery['gallery'] as $id => $item ) { ?>
          <li data-target="#bg-slider-container" data-slide-to="<?php echo $n; ?>" <?php echo ($n==0) ? 'class="active"' : ''; ?>></li>
         <?php $n++; } ?>
        </ol>
        <div class="carousel-inner">
        <?php $n=0; foreach( $gallery['gallery'] as $id => $item ) { ?>
          <div class="item <?php echo ($n==0) ? 'active' : ''; ?>">
            <img src="<?php echo $item['src']; ?>" id="<?php echo $id; ?>" title="<?php echo $item['title']; ?>" alt="<?php echo $item['alt']; ?>">
          </div>
        <?php $n++; } ?>
        </div>
        <a class="left carousel-control" href="#bg-slider-container" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#bg-slider-container" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
</div>
      </div>
   <?php } else { ?>
     <div class="bg"></div>
   <?php } ?>
   
   
     <div class="container">
       <div class="row">
        <div class="col-md-12 box-header">
 <?php
 
 
		if( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();
?>

 <h3 class="box-title"><?php the_title(); ?></h3>
            <div class="text-center"><?php the_content(); ?></div>
            
<?php
				endwhile;
		endif;
			?>        
			</div>
       </div> 
     </div>
     
     
<div class="container">
    <div class="row">
        <div class="col-md-12">
        
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">Promotions</li>
</ol>


        </div>
    </div>
</div>

			
    <div id="promotions-container">
        
         <div class="container">
            <div class="row">
                <div class="col-md-12">
                
                <div class="row promo-list">


<?php
// The Query

query_posts( array(
    'post_type' => 'promotion',
    'posts_per_page' => 12,
    'paged' => max( 1, get_query_var('paged') ),
) );

// The Loop
if ( have_posts() ) :
while ( have_posts() ) : the_post();
?>

            <div class="item-box col-md-3">
            <div class="thumbnail">
            <a href="<?php the_permalink(); ?>">
                 <?php
                       
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('medium', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:100%">
                        <?php
                        }
     ?>
     
           </a>
            </div>
             <h3 class="text-center"><?php the_title(); ?></h3>
             <p class="text-center"><?php echo get_post_meta(get_the_ID(), 'promo_subtitle', true); ?></p>
            </div>

<?php
endwhile;
endif;
?>


			            
                </div>
                
              
            </div>
            
        </div>  
       
       <hr>
        <!-- pagination -->

    <div class="row">
        <div class="col-md-12">
        
            <ul class="pagination">
             
              <?php
 
 global $wp_query;

$big = 999999999; // need an unlikely integer

$pagination = paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $wp_query->max_num_pages,
	'type' => 'array'
) );
if( $pagination ) {
foreach($pagination as $page) {
    echo "<li>" . $page ."</li>";
}
}
?>
             
              

            </ul>
        </div>
    </div>

       <!-- end pagination -->
 <?php
// Reset Query
wp_reset_query();
?>      
       
    </div>
    
 </div>
      
       
 <?php get_template_part('footer', 'partners'); ?>

</div>
<?php get_footer(); ?>

