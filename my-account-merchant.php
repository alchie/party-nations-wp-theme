<?php 
    global $current_user;
     get_currentuserinfo();
?>

<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#my-venue" data-toggle="tab">My Venue</a></li>
  <li><a href="#my-events" data-toggle="tab">My Events</a></li>
  <li><a href="#my-promotions" data-toggle="tab">My Promotions</a></li>  
  <li><a href="#my-reservations" data-toggle="tab">My Reservations</a></li>
    <li><a href="#my-reviews" data-toggle="tab">My Reviews</a></li>
</ul>

<div class="tab-content">
  <div class="tab-pane fade in active" id="my-venue">

<div class="venue-list">
<?php
/* ----------------------------------------------------------------------------*/

$args = array(
    'post_type' => 'venue',
    'posts_per_page' => of_get_option('partynations_user_post_limit'),
    'paged' => max( 1, get_query_var('paged') ),
    'author' => $current_user->ID
);
// The Query
query_posts( $args );

// The Loop
if ( have_posts() ) :

?>

           
          
<?php

while ( have_posts() ) : the_post();

?>


  
  
     <div class="item">
                <div class="row">

<div id="venue_details_<?php the_ID(); ?>" class="venue_details">

                    <div class="col-md-3">
                        <div class="thumbnail">

   <?php
                       
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('medium', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:100%">
                        <?php
                        }
     ?>
     
     </div>
     

                    </div>
                    <div class="col-md-9 details">

                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h3 class="panel-title"><span class="venue-title-<?php the_ID(); ?>"><?php the_title(); ?><span></h3>
                          </div>
                          <div class="panel-body">
                                     <span class="venue-description-<?php the_ID(); ?>"><?php the_excerpt(); ?></span>
                          </div>
                          <div class="panel-footer">
                            <a data-id="<?php the_ID(); ?>" href="#<?php the_ID(); ?>" class="btn btn-danger btn-sm btn-manage-venue">Manage <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
                          </div>
                        </div>  
   
                                          
                                          
                    </div>
                   
                </div>

</div>  


<div id="venue_update_<?php the_ID(); ?>" class="venue_update" style="display:none" data-id="<?php the_ID(); ?>">

<ul class="nav nav-tabs">
  <li class="active"><a href="#venue-details-tab-<?php the_ID(); ?>" data-toggle="tab">Details</a></li>
  <li><a href="#venue-image-tab-<?php the_ID(); ?>" data-toggle="tab">Image</a></li>
</ul>

<div class="tab-content">

    <div class="tab-pane fade in active" id="venue-details-tab-<?php the_ID(); ?>">
    
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Edit <span class="venue-title-<?php the_ID(); ?>"><?php the_title(); ?><span></h3>
  </div>
  <div class="panel-body">

  <div class="form-group" style="margin-top:10px">
    <label>Title</label>
    <input id="venue_title_<?php the_ID(); ?>" class="form-control input-<?php the_ID(); ?>" value="<?php the_title(); ?>">
  </div>
  <div class="form-group">
    <label>Description</label>
    <textarea id="venue_description_<?php the_ID(); ?>" class="form-control input-<?php the_ID(); ?>" style="height:100px;"><?php echo strip_tags( get_the_content() ); ?></textarea>
  </div>

<div class="form-group">
    <label>Location</label>
    <?php 
    $location = wp_get_post_terms( get_the_ID(), 'location', array("fields" => "ids") );
    $selected  = ( isset( $location[0] ) )  ? $location[0] : 0;
    wp_dropdown_categories( array(
        'taxonomy' => 'location',
        'hide_empty' => 0,
        'class'              => 'form-control input-' . get_the_ID(),
        'selected'           => $selected,
        'name' => 'venue_location_' .get_the_ID(),
    ) ); ?> 
  </div>
  
  <div class="form-group">
    <label>Opening Hours</label>
    <input id="venue_opening_<?php the_ID(); ?>" class="form-control input-<?php the_ID(); ?>" value="<?php echo get_post_meta(get_the_ID(), 'venue_opening', true); ?>">
  </div>
  
  <div class="form-group">
    <label>Happy Hours</label>
    <input id="venue_happy_<?php the_ID(); ?>" class="form-control input-<?php the_ID(); ?>" value="<?php echo get_post_meta(get_the_ID(), 'venue_happy', true); ?>">
  </div>
  
  <div class="form-group">
    <label>Website</label>
    <input id="venue_website_<?php the_ID(); ?>" class="form-control input-<?php the_ID(); ?>" value="<?php echo get_post_meta(get_the_ID(), 'venue_website', true); ?>">
  </div>
  
  <div class="form-group">
    <label>Phone</label>
    <input id="venue_phone_<?php the_ID(); ?>" class="form-control input-<?php the_ID(); ?>" value="<?php echo get_post_meta(get_the_ID(), 'venue_phone', true); ?>">
  </div>
  
  <div class="form-group">
    <label>Number of tables</label>
    <input id="venue_tables_<?php the_ID(); ?>" class="form-control input-<?php the_ID(); ?>" value="<?php echo get_post_meta(get_the_ID(), 'venue_tables', true); ?>">
  </div>
  
  </div>
  <div class="panel-footer">
     <button disabled="disabled" type="submit" class="btn btn-success btn-sm btn-venue-update submit-<?php the_ID(); ?>" data-id="<?php the_ID(); ?>" data-nonce="<?php echo wp_create_nonce( 'manage_venue_' . get_the_ID() ); ?>">Update <span class="glyphicon glyphicon-floppy-disk"></span></button> 
     <button type="button" class="btn btn-danger btn-sm btn-venue-cancel input-<?php the_ID(); ?>" data-id="<?php the_ID(); ?>">Cancel <span class="glyphicon glyphicon-remove"></span></button>
  </div>
</div>

<!-- end editor -->
</div> <!-- tab pane -->
<div class="tab-pane fade" id="venue-image-tab-<?php the_ID(); ?>">


                        
<center>
   <?php
                       
                       if ( has_post_thumbnail() ) {
	                        the_post_thumbnail('medium', array('class' => 'img-responsive') );
                        } else {
                        ?>
                             <img src="<?php echo get_template_directory_uri(); ?>/images/no-image-available.jpg" class="" style="width:100%">
                        <?php
                        }
     ?>
     </center>

    <form action="<?php echo admin_url( 'admin-ajax.php' ); ?>" method="post"
enctype="multipart/form-data">
<label for="file">Filename:</label>
<input type="file" name="file" id="file"><br>
<input type="hidden" name="action" value="update_venue_image">
<input type="hidden" name="id" value="<?php the_ID(); ?>">
<input type="hidden" name="nonce" value="<?php echo wp_create_nonce( 'manage_venue_' . get_the_ID() ); ?>">
 <button type="submit" class="btn btn-success btn-sm btn-venue-update submit-<?php the_ID(); ?>">Update <span class="glyphicon glyphicon-floppy-disk"></span></button> 
     <button type="button" class="btn btn-danger btn-sm btn-venue-cancel input-<?php the_ID(); ?>" data-id="<?php the_ID(); ?>">Cancel <span class="glyphicon glyphicon-remove"></span></button>
</form>


</div><!-- tab pane -->
</div><!-- tab-content -->
            
</div>
     </div>       
    
    
 <?php

endwhile;
?>

<?php
else:
    echo "<center>You don't have any venue! <a href=''>Add a Venue</a></center>";
endif;

/* ----------------------------------------------------------------------------*/
?>
 </div>
  
  </div>
  <div class="tab-pane fade" id="my-events">
  
  <?php
/* ----------------------------------------------------------------------------*/

$args = array(
    'post_type' => 'event',
    'posts_per_page' => -1,
    'paged' => max( 1, get_query_var('paged') ),
    'author' => $current_user->ID
);
// The Query
query_posts( $args );

// The Loop
if ( have_posts() ) :

?>

 <div class="table-responsive" style="padding:10px;">
<table class="table table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th width="90px">Actions</th>
          </tr>
        </thead>
        <tbody>
          
          
<?php

while ( have_posts() ) : the_post();

?>


    <tr>
            <td><?php the_ID(); ?></td>
            <td><?php the_title(); ?></td>
            <td><?php the_excerpt(); ?></td>
            <td><a href="">Edit</a> &middot; <a href="#delete">Delete</a></td>
          </tr>
    
    
 <?php

endwhile;
?>
</tbody>
      </table>
</div>
<?php
else:
    echo "<center>You have not posted any event!</center>";
endif;

/* ----------------------------------------------------------------------------*/
?>
  
  </div>
  <div class="tab-pane fade" id="my-promotions">
  
  <?php
/* ----------------------------------------------------------------------------*/

$args = array(
    'post_type' => 'promotion',
    'posts_per_page' => -1,
    'paged' => max( 1, get_query_var('paged') ),
    'author' => $current_user->ID
);
// The Query
query_posts( $args );

// The Loop
if ( have_posts() ) :

?>

 <div class="table-responsive" style="padding:10px;">
<table class="table table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th width="90px">Actions</th>
          </tr>
        </thead>
        <tbody>
          
          
<?php

while ( have_posts() ) : the_post();

?>


    <tr>
            <td><?php the_ID(); ?></td>
            <td><?php the_title(); ?></td>
            <td><?php the_excerpt(); ?></td>
            <td><a href="">Edit</a> &middot; <a href="#delete">Delete</a></td>
          </tr>
    
    
 <?php

endwhile;
?>
</tbody>
      </table>
</div>
<?php
else:
    echo "<center>You have not posted any promotion!</center>";
endif;

/* ----------------------------------------------------------------------------*/
?>
  
  </div>
  
   <div class="tab-pane fade" id="my-reservations">
  
  <?php
/* ----------------------------------------------------------------------------*/

$reservations = getTableReservations( $current_user->ID );

// The Loop
if ( count( $reservations ) > 0 ) :

?>

 <div class="table-responsive" style="padding:10px;">
<table class="table table-hover">
        <thead>
          <tr>
            <th>Venue</th>
            <th>Table Number</th>
            <th>Date</th>
            <th width="90px">Actions</th>
          </tr>
        </thead>
        <tbody>
          
          
<?php

foreach($reservations as $reservation) :

?>


    <tr>
            <td><a href="<?php echo get_permalink( $reservation->post_id ); ?>"><?php echo get_the_title( $reservation->post_id ); ?></a></td>
            <td><?php echo $reservation->table_number; ?></td>
            <td><?php echo $reservation->sched_date; ?></td>
            <td><a href="#cancel">Cancel</a></td>
          </tr>
    
    
 <?php

endforeach;
?>
</tbody>
      </table>
</div>
<?php
else:
    echo "<center>You have not posted any review!</center>";
endif;

/* ----------------------------------------------------------------------------*/
?>
  
  
    </div>
  
  
  <div class="tab-pane fade" id="my-reviews">

<?php
/* ----------------------------------------------------------------------------*/

$comments = get_comments('user_id=' . $current_user->ID );

// The Loop
if ( count( $comments ) > 0 ) :

?>

 <div class="table-responsive" style="padding:10px;">
<table class="table table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Post</th>
            <th>Title</th>
            <th>Review</th>
            <th width="90px">Actions</th>
          </tr>
        </thead>
        <tbody>
          
          
<?php

foreach($comments as $comment) :

?>


    <tr>
            <td><?php echo $comment->comment_ID; ?></td>
            <td><?php echo get_post_type( $comment->comment_post_ID ) ?></td>
            <td><?php echo get_the_title( $comment->comment_post_ID ) ?></td>
            <td><?php echo $comment->comment_content; ?></td>
            <td><a href="">Edit</a> &middot; <a href="#delete">Delete</a></td>
          </tr>
    
    
 <?php

endforeach;
?>
</tbody>
      </table>
</div>
<?php
else:
    echo "<center>You have not posted any review!</center>";
endif;

/* ----------------------------------------------------------------------------*/
?>

  </div>
  
</div>
